#pragma once

#include "ListTool2B.h"
#include "Konstanter.h"
#include <fstream>


//	Nasjon-klassen sine datamedlemmer og funksjonsheadinger
class Nasjon : public TextElement
{
private:
	//	Nasjonsforkortelse lagres i TextElement
	char* nasjonNavn;		//	Nasjonens fulle navn
	int antDeltagere;		//	Antall deltagere i nasjonens tropp
	char* personNavn;		//	Kontaktpersonens navn
	int personTlf;			//	Kontaktpersonens telefonnummer
	char* genData;			//	Generell informasjon om nasjonen, trivia
public:
	Nasjon();
	Nasjon(char ID[NASJONIDLENGDE+1]);
	Nasjon(std::ifstream & inn, char ID[NASJONIDLENGDE + 1]);
	~Nasjon();
	virtual void display();
	//void displayTropp(char* ID);
	void displayHoved();		//	display hoveddata for en nasjon
	const char* hentNasjonID();
	void nyDeltager();
	void endreData();
	void nasjonSkrivTilFil(std::ofstream & ut);
};
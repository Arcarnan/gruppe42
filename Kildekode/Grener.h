#pragma once

#include "ListTool2B.h"
#include "Gren.h"

class Grener														
{																	
private:															
	List* grenerList;											//	liste av grener
public:															
	Grener();													//	constructor
	~Grener();													//	destructor
	bool finnesGren(char* grenNvn);								//	sjekker om gren finnes
	bool finnesOvelse(char* grenNvn, int tempID);				//	returnerer ovelsens plass i listen om den finnes
	void nyGren();												//	registrerer ny gren
	void display();												//	viser data for en spesifik gren
	void displayHoved();										//	viser hoveddata for gren pa skjerm
	void displayOvelser(char* grenNvn);							//	viser data for ovelser
	void endreGren();											//	endre data for en gren
	void grenerLesFraFil();										//	leser greners data fra fil
	void grenerSkrivTilFil();									//	skriver greners data til fil
	void nyOvelse(char* grenNavn);								//	registrerer ny ovelse
	void endreOvelse(char* grenNavn);							//	endre data for en ovelse
	void slettOvelse(char* grenNavn);							//	slette en ovelse
	void skrivResultatListe(char* grenNavn, int ovelseId);		//	viser resultatliste for en ovelse pa skjerm
	void nyResultatListe(char* grenNavn, int ovelseId);			//	registrerer en ny resultatliste for ovelse
	void slettResultatListe(char* grenNavn, int ovelseId);		//	sletter resultatlisten for en ovelse
	void skrivDeltagerStartliste(char* grenNvn, int tempID);	//	viser deltagerliste for en ovelse
	void nyDeltagerStartliste(char* grenNvn, int tempID);		//	registrerer ny deltagerliste for en ovelse
	void endreDeltagerStartliste(char* grenNvn, int tempID);	//	endre deltagerliste for en ovelse
	void slettDeltagerStartliste(char* grenNvn, int tempID);	//	slett deltagerliste for en ovelse
};
#pragma once

enum KJONN
{
	gutt, jente
};

const int STRLEN = 100;
const int NASJONIDLENGDE = 3;
const int MAXNASJONER = 200;		//	endret fra 50 til 200, siden "Statistikk � inneholder opptil 200 nasjonsforkortelser. "
const int MINOVELSER = 1;
const int MAXOVELSERPERGREN = 20;	//	Max antall ovelser i en spesifikk gren
const int MAXOVELSEID = 9999;		//	Max antall ovelser i programmet totalt
const int MAXGRENER = 50;			//	sa ikke noe om maxgrener, sa gjetter 50
const int MINDELTAGERE = 1;
const int MAXDELTAGERE = 100;
const int DATOLEN = 8;
const int TIDLEN = 4;
const int MINKLOKKESLETT = 0000;
const int MAXKLOKKESLETT = 2359;
const int MAXPOENGTID = 999999;
const int MAXNUMERISK = 30;		    //	max antall siffre
const int MINDELTAGERNUMMER = 100;
const int MAXDELTAGERNUMMER = 10000;
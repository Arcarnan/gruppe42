#pragma once

#include "ListTool2B.h"
#include "konstanter.h"

class Ovelse
{
private:
	int ovelseID;											//	ovelsens ID
	char* fulltNavn;										//	ovelsens fulle navn
	int antallDeltagere;									//	ovelsens antall deltagere
	char dato[DATOLEN + 1];									//	ovelsens dato (med punktum)
	char klokkeslett[TIDLEN + 1];							//	ovelsens klokkeslett
	char deltagerlisteFilnavn[11];							//	filnavnet for ovelsens deltagere
	char resultatlisteFilnavn[11];							//	filnavnet for ovelsens resultater
public:
	Ovelse(int ovlsID);										//	constructor
	Ovelse(std::ifstream &inn, int antSifre);								//	constructor, leser fra fil
	int hentOvelseID();										//	returnerer ovelsens ID
	const char* hentOvelseNavn();							//	returnerer ovelsens navn
	void endreOvelseData();									//	tilbyr bruker a endre data for ovelse
	void display();											//	skriver ut data for ovelse
	void ovelseSkrivTilFil(std::ofstream & ut);				//	skriver ovelses data til fil
	void skrivResultatListe(int antallSiffre);				//	viser resultatlistens innhold pa skjerm
	void nyResultatListe(int antallSiffre);					//	registrrer ny resultatliste
	void slettResultatliste(int antallSiffre);				//	sletter resultatliste
	void skrivDeltagerStartliste();							//	viser deltagerliste pa skjerm
	void nyDeltagerStartliste();                			//	registrerer ny deltagerliste
	void endreDeltagerStartliste();							//	endrer deltagere i startliste
	void slettDeltagerStartliste();							//	sletter startliste
	void sorterLister(int* deltagere, int* resultat, int antallSiffre);		//	Sorterer listene i samme rekkefolge
	void lesListerFraFil(int* deltagere, int* resultat);	//	leser deltager- og resultatliste fra fil
	void skrivListerTilFil(int* deltagere, int* resultat);	//	skriver deltager- og resultatliste til fil
    void redigerPoeng(int antSifre, bool leggeTil);
	~Ovelse();												//  ovelses destructor
};
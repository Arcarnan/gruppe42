#pragma once
#include <fstream>
#include "konstanter.h"

class Statistikk
{
	protected:
		char nasjonsforkortelser[MAXNASJONER+1][NASJONIDLENGDE+1];
		int antallNasjoner;
        void skrivNasjonForkortelse(int n);
	public:
		Statistikk();
		void lesFraFil(std::ifstream &inn);
		void skrivTilFil(std::ofstream & ut, int nr);
		
};


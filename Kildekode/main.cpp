#pragma once

//  INCLUDE:
#include <iostream>				//  cin, cout
#include <fstream>				//  ifstream, ofstream
#include <cstring>				//  strcpy, strlen, strncmp
#include <cctype>				//  toupper

#include "konstanter.h"			//  Conster
#include "ListTool2B.h"			//  List tool
#include "Hjelpefunksjoner.h"	//	hjelpefunksjoner
#include "Nasjoner.h"
#include "Nasjon.h"
#include "Grener.h"
#include "Medaljer.h"
#include "Poeng.h"
#include "Menyvalg.h"
#include "Deltagere.h"


//  GLOBALE VARIABLE:

// DETTE FRODE BRUKER I SIN MAIN
Nasjoner nasjonbase;
Deltagere deltagerbase;
Grener grenbase;
Medaljer medaljestatistikk;
Poeng poengstatistikk;



int main()
{
	char kommando;							     //leser kommando fra bruker

	//  Mulig at det er unodvendig aa lese inn fra fil to ganger. Skjer ogsaa i construktor.
	//			----skjer ikke i konstruktor, mulig � implementere men er dette nodvendig?

	nasjonbase.nasjonerLesFraFil();
    deltagerbase.lesFraFil();
	grenbase.grenerLesFraFil();				
	//medaljestatistikk.lesFraFil();        //  unodvendig a ha les fra fil funksjon for dette, oppdateres fra resultatlister			   
    //poengstatistikk.lesFraFil();			//  unodvendig a ha les fra fil funksjon for dette, oppdateres fra resultatlister	

	skrivLitenMeny();

	kommando = les("Hovedmeny: Hva vil du gjore?");						     // leser kommando fra bruker

	while (kommando != 'X')
	{
		switch (kommando)
		{
		case 'N':								//  nasjon
			valgN();
			break;

		case 'D':								//  deltager
			valgD();
			break;

		case 'G':							    //  gren
			valgG();
			break;

		case 'O':								//  ovelse
			valgO();
			break;

		case 'M':  						        //  Skriver hele medaljeoversikten/-statistikken p� skjermen. 
			medaljestatistikk.medaljeOversikt();
			break;

		case 'P':  						        //  Skriver hele poengoversikten/-statistikken pa skjermen. 
			poengstatistikk.poengOversikt();
			break;

		case 'H':                               //  skriver hele menyen til skjerm
			skrivMeny();
			break;

		default:
			std::cout << "\n\n\tUgyldig kommando! Trykk <H> for full oversikt over menyvalg. \n\n";
			skrivLitenMeny();
			break;
		}
		kommando = les("Hovedmeny: Hva vil du gjore?");						     //leser kommando fra bruker
	}
	medaljestatistikk.skrivTilFil();		//midlertidig testing.
	poengstatistikk.skrivTilFil();		    //midlertidig testing.
	deltagerbase.skrivTilFil();
	nasjonbase.nasjonerSkrivTilFil();
	grenbase.grenerSkrivTilFil();
	return 0;
}
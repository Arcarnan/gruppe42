#pragma once		//erstatter "if !defined" --- "define"

#include "Statistikk.h"
#include "konstanter.h"


class Medaljer : public Statistikk
{
	private:
		int medaljer[MAXNASJONER+1][3];
		int antallNasjonerMedMedaljer;
	public:
	Medaljer();
	//void lesFraFil();
	void skrivTilFil();
	void medaljeOversikt();
    void sorterLister(char poengNasjonsForkortelser[MAXNASJONER + 1][NASJONIDLENGDE + 1], int* poeng, int& poengsNasjoner, int& antallMedPoeng);
    void redigerMedalje(char* nasjonsID, int medaljetype, bool skalHa);
};


#pragma once
#include "Statistikk.h"
#include <iostream>

	//		private:
	//			char* nasjonsforkortelser[200];
	//			int antallNasjoner;

Statistikk::Statistikk()
{
	antallNasjoner = 0;
}

void Statistikk::lesFraFil(std::ifstream & inn)
{
	antallNasjoner++;

	inn.getline(nasjonsforkortelser[antallNasjoner], NASJONIDLENGDE+1);	//automaticly ignores (1);
}

void Statistikk::skrivTilFil(std::ofstream & ut, int nr)
{
	ut << nasjonsforkortelser[nr];
}

void  Statistikk::skrivNasjonForkortelse(int n)
{	
	std::cout << "\n " << nasjonsforkortelser[n];
}
#include <iostream>				//  cin, cout
#include <fstream>				//  ifstream, ofstream
#include <cstring>				//  strcpy, strlen, strncmp
#include "Gren.h"				//	inkluderer h-filen
#include "Hjelpefunksjoner.h"	// Les().
#include "Konstanter.h"

Gren::Gren()
{														//	skriver ut en melding pa skjermen om denne noen gang skulle komme til a brukes
	std::cout << "\nDette er en tom constructor for Gren, som ikke er ment aa brukes. ";
}

Gren::Gren(char* grenNvn) : TextElement(grenNvn)
{
	int poengType;										 //leser kommando fra bruker

	std::cout << "\nHva maales prestasjoner i? "
		<< "\n\t <1> Poeng: 1 - 9. "
		<< "\n\t <2> Poeng: 10-99. "
		<< "\n\t <3> Tid:   MM:SS:T. "
		<< "\n\t <4> Tid:   MM:SS:HH. "
		<< "\n\t <5> Tid:   MM:SS:TTT. ";
	poengType = les("Prestasjonstype:", 1, 5);			  //leser kommando fra bruker

	switch (poengType)
	{													//	registrerer bade type maling og antall sifre samtidig
	case 1:
		typeMaaling = 'P';
		antSifre = 1;
		break;

	case 2:
		typeMaaling = 'P';
		antSifre = 2;
		break;

	case 3:
		typeMaaling = 'T';
		antSifre = 5;
		break;

	case 4:
		typeMaaling = 'T';
		antSifre = 6;
		break;

	case 5:
		typeMaaling = 'T';
		antSifre = 7;
		break;

	default: std::cout << "\n\t\tDette er desverre et ugyldig valg. ";

	}
	antOvelser = 0;									//	nar gren lages har den for oyeblikket ingen ovelser

	for (int i = 0; i <= MAXOVELSERPERGREN; i++)
	{												//	nullstiller array med pekere nar gren lages
		ovelsePtr[i] = nullptr;
	}
}

Gren::Gren(std::ifstream & inn, char* grenNvn) : TextElement(grenNvn)
{
	inn >> typeMaaling;
	inn.ignore();

	inn >> antSifre;
	inn.ignore();

	inn >> antOvelser;
	inn.ignore();

	ovelsePtr[0] = nullptr;
	for (int i = 1; i <= MAXOVELSERPERGREN; i++)
	{													//	leser inn ovelser for grenen, og setter resten til nullptr
		if (i <= antOvelser)
		{
			ovelsePtr[i] = new Ovelse(inn, antSifre);
		}
		else
		{
			ovelsePtr[i] = nullptr;
		}

	}
}

Gren::~Gren()
{
	for (int i = 1; i <= antOvelser; i++)
	{
		delete ovelsePtr[i];
	}
}

void Gren::display()
{
	std::cout << "\n\nGren: " << text;
	std::cout << "\nAntall ovelser: " << antOvelser;
	std::cout << "\nResultat type (Poeng/ tid): " << typeMaaling;

	for (int i = 1; i <= antOvelser; i++)
	{												//	kjorer display funksjonen til alle ovelser
		ovelsePtr[i]->display();
	}
}

void Gren::displayHoved()
{
	std::cout << "\n\nGren: " << text;
	std::cout << "\nAntall ovelser: " << antOvelser;
	std::cout << "\nResultat type: " << typeMaaling;
}

void Gren::displayOvelser()
{
	for (int i = 1; i <= antOvelser; i++)
	{												//	kjorer display funksjonen til alle ovelser
		ovelsePtr[i]->display();
	}
}

void Gren::endreData()
{
	char tempStr[STRLEN];
	int tempLen;

	std::cout << "\nGrenens navn for oyeblikket: " << text;		//	skriver ut grenens navn
	delete[] text;												//	sletter grenens navn

	les("\nGrenens nye navn: ", tempStr);						//	registrerer nytt navn for gren
	tempLen = strlen(tempStr) + 1;								//	Maler lengden med \0
	text = new char[tempLen];
	strcpy_s(text, tempLen, tempStr);
}

void Gren::endreOvelseData()
{
	int tempID;

	tempID = les("Unik ovelse ID", MINOVELSER, MAXOVELSEID);

	if (finnesAllerede(tempID) != 0)							//	hvis ovelses ID en finnes
	{
		ovelsePtr[finnesAllerede(tempID)]->endreOvelseData();
	}
	else
	{
		std::cout << "\nDenne ovelsen finnes ikke i systemet. ";
	}
}

void Gren::grenSkrivTilFil(std::ofstream & ut)
{
	ut << "\n" << text << "\n"									//	starter med \n for a ha et linjehopp mellom hver gren
		<< typeMaaling << "\n"
		<< antSifre << "\n"
		<< antOvelser << "\n";

	for (int i = 1; i <= antOvelser; i++)
	{
		ovelsePtr[i]->ovelseSkrivTilFil(ut);
	}
}

void Gren::nyOvelse(int tempOvelseID)
{
	if (antOvelser < MAXOVELSERPERGREN)										//	dersom det er plass til flere
	{
		//  tempID = les("Unik ovelse ID", MINOVELSER, MAXOVELSEID);

		if (finnesAllerede(tempOvelseID) != 0)									//	dersom ovelsens ID allerede eksisterer
		{
			std::cout << "\nDenne ID en er allerede i bruk for ovelse: "	//	skriver ut ovelsens navn
				<< ovelsePtr[finnesAllerede(tempOvelseID)]->hentOvelseNavn();
		}
		else
		{
			antOvelser += 1;
			ovelsePtr[antOvelser] = new Ovelse(tempOvelseID);						//	antall ovelser opdateres hver gang en ny ovelse registreres
		}
	}
	else
	{
		std::cout << "\nFullt av ovelser for gren: " << text;
	}
}

void Gren::endreOvelse()
{
	int tempID;

	tempID = les("Unik ovelse ID", MINOVELSER, MAXOVELSEID);

	if (finnesAllerede(tempID) != 0)										//	dersom ovelses ID-en finnes
	{
		ovelsePtr[finnesAllerede(tempID)]->endreOvelseData();
	}
	else
	{
		std::cout << "\nDenne ovelsen finnes ikke i systemet. ";
	}
}

int Gren::finnesAllerede(int ovlsID)
{
	for (int i = 1; i <= antOvelser; i++)
	{
		if (ovelsePtr[i]->hentOvelseID() == ovlsID)
		{
			return i;														//	returnerer ovelseplassen det ovelsen eksisterer
		}
	}
	return 0;																//	returnerer 0 om ovelse ID ikke finnes
}

void Gren::slettOvelse()
{
	int tempID;
	char kommando;

	tempID = les("Unik ovelse ID", MINOVELSER, MAXOVELSEID);

	if (finnesAllerede(tempID) != 0)										//	sjekker at ovelses ID faktisk eksisterer
	{
		std::cout << "\nEr du sikker pa at du vil slette ovelse " << tempID << ", " << ovelsePtr[finnesAllerede(tempID)]->hentOvelseNavn() << "? ";
		kommando = les("<Y> / <N>");										//	dobbeltsjekker at brukeren faktisk vil slette ovelsen

		if (kommando == 'Y')
		{						
			delete ovelsePtr[finnesAllerede(tempID)];						//	sletter ovelsen

			for (int i = finnesAllerede(tempID); i <= antOvelser; i++)		//	flytter alle resterende ovelser et hakk innover i array
			{
				ovelsePtr[i] = ovelsePtr[i + 1];
			}
			antOvelser--;													//	reduserer antall ovelser med 1

			ovelsePtr[finnesAllerede(tempID)]->slettResultatliste(antSifre);	//	passer her pa at resultatliste ogsa slettes, og statistikk oppdateres
			ovelsePtr[finnesAllerede(tempID)]->slettDeltagerStartliste();		//	passer her pa at deltagerliste ogsa slettes
		}
		else
		{
			std::cout << "\nOperasjon avbrutt, ovelse IKKE slettet. ";
		}
	}
	else
	{
		std::cout << "\nDenne ovelsen finnes ikke i systemet. ";
	}
}

bool Gren::finnesOvelse(int ovelseID)										//	sjekker om ovelsen eksisterer
{
	for (int i = 1; i <= antOvelser; i++)
	{
		if (ovelsePtr[i]->hentOvelseID() == ovelseID)
		{
			return true;
		}
	}
	return false;
}

void Gren::skrivResultatListe(int ovelseId)
{
	ovelsePtr[finnesAllerede(ovelseId)]->skrivResultatListe(antSifre);
}

void Gren::nyResultatListe(int ovelseId)
{
	ovelsePtr[finnesAllerede(ovelseId)]->nyResultatListe(antSifre);
}

void Gren::slettResultatliste(int ovelsesId)
{
	ovelsePtr[finnesAllerede(ovelsesId)]->slettResultatliste(antSifre);
}

void Gren::skrivDeltagerStartliste(int ovlsID)
{
	ovelsePtr[finnesAllerede(ovlsID)]->skrivDeltagerStartliste();
}

void Gren::nyDeltagerStartliste(int ovlsID)
{
	ovelsePtr[finnesAllerede(ovlsID)]->nyDeltagerStartliste();
}

void Gren::endreDeltagerStartliste(int ovlsID)
{
	ovelsePtr[finnesAllerede(ovlsID)]->endreDeltagerStartliste();
}

void Gren::slettDeltagerStartliste(int ovlsID)
{
	ovelsePtr[finnesAllerede(ovlsID)]->slettDeltagerStartliste();
}

/*
		FILFORMAT:
Antall Grener

Gren navn
type maaling
antall sifre i maalingen
antall ovelser
ovelse1 - ovelsens ID
ovelse1 - ovelsens fulle navn
ovelse1 - dato for ovelsen
ovelse1 - klokkeslett for ovelsen
ovelse1 - antall deltagere
ovelse2 - ovelsens ID
ovelse2 - ovelsens fulle navn
ovelse2 - dato for ovelsen
ovelse2 - klokkeslett for ovelsen
ovelse2 - antall deltagere

Gren navn
type maaling
antall sifre i maalingen
antall ovelser
*/
//  INCLUDE:
#include <iostream>				//  cin, cout
#include <fstream>				//  ifstream, ofstream
#include <cstring>				//  strcpy, strlen, strncmp
#include <cctype>				//  toupper
#include "Menyvalg.h"
#include "Hjelpefunksjoner.h"	//	les, 
#include "konstanter.h"
#include "Nasjoner.h"
#include "Grener.h"
#include "Deltagere.h"
#include "Medaljer.h"
#include "Poeng.h"

extern Nasjoner nasjonbase;
extern Grener grenbase;
extern Deltagere deltagerbase;
extern Medaljer medaljestatistikk;
extern Poeng poengstatistikk;

void valgN()								   	 
{
	char kommando;							     //leser kommando fra bruker

	std::cout << "\n\t\tFolgende underkommandoer er tilgjengelige for <N>: \n";
	std::cout << "\n\t<N> - Registrer en ny nasjon. ";
	std::cout << "\n\t<E> - Endre en nasjon. ";
	std::cout << "\n\t<A> - Skriv hoveddataene om alle nasjoner. ";
	std::cout << "\n\t<T> - Skriv en nasjons deltagertropp. ";
	std::cout << "\n\t<S> - Skriv alle data om en gitt nasjon. \n";
	std::cout << "\n\t<X> - Tilbake til hovedmeny. \n";

	kommando = les("Nasjon: Hva vil du gjore?");					//leser kommando fra bruker

	while (kommando != 'X')
	{
		switch (kommando)
		{
		case 'N':							//Alle dataene om en ny og unik nasjon leses inn
			nasjonbase.nyNasjon();
			break;
		case 'E':							//Alle data om en nasjon (unntatt dets forkortelse og antallet i troppen) kan endres.
			nasjonbase.endreNasjon();
			break;
		case 'A': 							//Forkortelsen, hele navnet og antallet i troppen skrives ut for alle nasjonene.
			nasjonbase.displayHoved();
			break;
		case 'T': 							//Nummer, navn og kjonn om alle deltagerne i aktuell nasjons tropp skrives ut.
			deltagerbase.displayNasjonTropp();
			break;
		case 'S': 							//Skriver alle data om en nasjon
			nasjonbase.display();
			break;
        default:
            std::cout << "\nDette er desverre ikke et gyldig valg. \n";
            break;
		}
		kommando = les("Nasjon: Hva vil du gjore?");   //leser kommando fra bruker
	}
	nasjonbase.nasjonerSkrivTilFil();
}

void valgD()								     
{
	char kommando;							     //leser kommando fra bruker

	std::cout << "\n\t\tFolgende underkommandoer er tilgjengelige for <D>: \n";
	std::cout << "\n\t<N> - Registrer en ny deltager. ";
	std::cout << "\n\t<E> - Endre en deltager. ";
	std::cout << "\n\t<A> - Skriv hoveddataene om alle deltagere. ";
	std::cout << "\n\t<S> - Skriv alle data om en gitt deltager. \n";
	std::cout << "\n\t<X> - Tilbake til hovedmeny. \n";

	kommando = les("Deltager: Hva vil du gjore?");					//leser kommando fra bruker

	while (kommando != 'X')
	{

		switch (kommando)
		{
		case 'N':							//Alle dataene om en ny og unik deltager leses inn. (Det ma selvsagt sjekkes at nasjonen virkelig eksisterer.)
			deltagerbase.leggTilDeltager();
			break;
		case 'E':							//Alle data om en deltager (unntatt vedkommendes nummer) kan endres. 
			deltagerbase.endreEnDeltager();
			break;
		case 'A': 							//Nummer, navn og kjonn om alle deltagerne skrives ut. 
			deltagerbase.skrivHoveddata();
			break;
		case 'S': 							//Brukeren kan velge a angi deltageren vha. vedkommendes unike nummer eller vha. (en entydig del av) navnet.
			deltagerbase.skrivDeltager();
			break;
        default:
            std::cout << "\nDette er desverre ikke et gyldig valg. \n";
            break;
		}
		kommando = les("Deltager: Hva vil du gjore?");							     //leser kommando fra bruker
		deltagerbase.skrivTilFil();
	}
}

void valgG()									 
{
	char kommando;							     //leser kommando fra bruker

	std::cout << "\n\t\tFolgende underkommandoer er tilgjengelige for <G>: \n";
	std::cout << "\n\t<N> - Registrer en ny gren. ";
	std::cout << "\n\t<E> - Endre en gren. ";
	std::cout << "\n\t<A> - Skriv hoveddataene om alle grener. ";
	std::cout << "\n\t<S> - Skriv alle data om en gitt gren. \n";
	std::cout << "\n\t<X> - Tilbake til hovedmeny. \n";

	kommando = les("Gren: Hva vil du gjore?");					//leser kommando fra bruker

	while (kommando != 'X')
	{
		switch (kommando)
		{
		case 'N':							//Alle dataene om en ny og unik gren leses inn. (Det skal ikke leses inn noen ovelser her � det gjores vha.kommandoen O N )
			grenbase.nyGren();
			break;
		case 'E':							//Kun grenens unike navn tilbys a bli endret. 
			grenbase.endreGren();
			break;
		case 'A':  							//Navn, registreringen av prestasjoner (tid/poeng) og antall ovelser om alle grenene skrives ut.
			grenbase.displayHoved();
			break;
		case 'S': 							//Grenens navn, registreringen av prestasjoner (tid/poeng), samt nummer, fullt navn, dato, klokkeslett og antall deltagere i alle ovelsene skrives ut.
			grenbase.display();
			break;
        default:
            std::cout << "\nDette er desverre ikke et gyldig valg. \n";
            break;
		}
		kommando = les("Gren: Hva vil du gjore?");							     //leser kommando fra bruker
	}
	grenbase.grenerSkrivTilFil();
}

void valgO()
{
	char grenNavn[STRLEN];

	les ("Gren navn", grenNavn);

	if (grenbase.finnesGren(grenNavn))
	{
		char kommando;							     //leser kommando fra bruker

		std::cout << "\n\t\tFolgende underkommandoer er tilgjengelige for <O>: \n";
		std::cout << "\n\t<N> - Registrer en ny ovelse. ";
		std::cout << "\n\t<E> - Endre en ovelse. ";
		std::cout << "\n\t<F> - Fjerne/slette en ovelse. ";
		std::cout << "\n\t<A> - Skriv hoveddataene om alle ovelser. ";
		std::cout << "\n\t<L> Lister:   <ovelse(id)> - <S> - Skriv deltager-/startliste. ";
		std::cout << "\n\t	      <ovelse(id)> - <N> - Ny deltager-/startliste. ";
		std::cout << "\n\t	      <ovelse(id)> - <E> - Endre deltager-/startliste. ";
		std::cout << "\n\t	      <ovelse(id)> - <F> - Fjerne/slette deltager-/startliste. ";

		std::cout << "\n\t<R> Resultat: <ovelse(id)> - <S> - Skriv resultatliste. ";
		std::cout << "\n\t	      <ovelse(id)> - <N> - Ny resultatliste. ";
		std::cout << "\n\t	      <ovelse(id)> - <F> - Fjerne/slette resultatliste. \n";
		std::cout << "\n\t\t<X> - Tilbake til hovedmeny. \n";

		kommando = les("Ovelse: Hva vil du gjore?");	//leser kommando fra bruker

		while (kommando != 'X')
		{

			switch (kommando)
			{
			case 'N':								//Leser inn (om plass og ikke finnes allerede under aktuell gren) alle data om en ny ovelse. (Leser ikke inn deltager-/startliste og resultatliste. Dette utfores vha. kommandoene �L N� og �R N�.)
				grenbase.nyOvelse(grenNavn);		//sender med grennavn
				break;
			case 'E':								//Navn, dato og klokkeslett kan endres (ikke alt det andre). 
				grenbase.endreOvelse(grenNavn);		//sender med grennavn
				break;
			case 'F': 								//onsket ovelse fjernes helt. NB: Dette er litt tricky, for: Husk a fjerne involverte filer, samt a oppdateres medalje- og poengstatistikken.
				grenbase.slettOvelse(grenNavn);		//sender med grennavn	//--path stemmer--funksjon ikke ferdig
				break;
			case 'A': 								//Det samme som ved kommandoen �G S� skrives. 
				grenbase.displayOvelser(grenNavn);	//sender med grennavn
				break;
			case 'L':  								//lister
				valgL(grenNavn);					//sender med grennavn
				break;
			case 'R':  								//resultat
				valgR(grenNavn);					//sender med grennavn
				break;
			}
			kommando = les("Ovelse: Hva vil du gjore?");	 //leser kommando fra bruker
		}
		grenbase.grenerSkrivTilFil();
	}
	else
	{
		std::cout << "\nDenne grenen finnes ikke i systemet. ";
	}
}

void valgL(char* grenNavn)						 //	mottar grennavn
{
	int tempOvelseID;
	char kommando;							     //	leser kommando fra bruker

	tempOvelseID = les("\nOvelsens ID: ", MINOVELSER, MAXOVELSEID);

	if (grenbase.finnesOvelse(grenNavn, tempOvelseID))
	{
		std::cout << "\n\t\tFolgende underkommandoer er tilgjengelige for <L>: \n";
		std::cout << "\n\t<S> - Skriv deltager-/startliste. ";
		std::cout << "\n\t<N> - Ny deltager-/startliste. ";
		std::cout << "\n\t<E> - Endre deltager-/startliste. ";
		std::cout << "\n\t<F> - Fjerne/slette deltager-/startliste. \n";
		std::cout << "\n\t<X> - Tilbake til hovedmeny. \n";

		kommando = les("Deltager/ startlister: Hva vil du gjore?");					//	leser kommando fra bruker

		while (kommando != 'X')
		{

			switch (kommando)
			{
			case 'S': 														//	Om deltagerliste finnes, sa leses denne inn fra fil, og skrives pa skjermen (startnummer, samt deltagernes nummer, navn og nasjon).
				grenbase.skrivDeltagerStartliste(grenNavn, tempOvelseID);	//	sender med grennavn og ovelse ID
				break;
			case 'N':														//	Om deltagerliste ikke finnes, sa leses en ny inn fra tastaturet, og skrives deretter til fil. Husk a sjekke at deltagerne virkelig finnes.
				grenbase.nyDeltagerStartliste(grenNavn, tempOvelseID);		//	sender med grennavn og ovelse ID
				break;
			case 'E':														//	Om resultatliste ikke allerede finnes, sa tilbys brukeren a endre pa deltager-/startlisten. Den blir forst lest inn fra fil, og skrevet tilbake igjen etterpa.
				grenbase.endreDeltagerStartliste (grenNavn, tempOvelseID);	//	sender med grennavn og ovelse ID
				break;
			case 'F': 														//	Om resultatliste ikke allerede finnes, sa tilbys brukeren a slette/fjerne hele deltager-/startlisten (altsa dets fil).
				grenbase.slettDeltagerStartliste(grenNavn, tempOvelseID);	//	sender med grennavn og ovelse ID
				break;
			}
			kommando = les("Deltager/ startlister: Hva vil du gjore?");					//	leser kommando fra bruker
		}
	}
}

void valgR(char* grenNavn)					 	//	mottar grennavn
{
	int tempOvelseID;
	char kommando;							   //	leser kommando fra bruker

	tempOvelseID = les("\nOvelsens ID: ", MINOVELSER, MAXOVELSEID);

	if (grenbase.finnesOvelse(grenNavn, tempOvelseID))
	{
		std::cout << "\n\t\tFolgende underkommandoer er tilgjengelige for <R>: \n";
		std::cout << "\n\t<S> - Skriv resultatliste. ";
		std::cout << "\n\t<N> - Ny resultatliste. ";
		std::cout << "\n\t<F> - Fjerne/slette resultatliste. \n";
		std::cout << "\n\t<X> - Tilbake til hovedmeny. \n";

		kommando = les("Resultat: Hva vil du gjore?");					//leser kommando fra bruker

		while (kommando != 'X')
		{

			switch (kommando)
			{
			case 'S':													//Om resultatliste finnes, sa leses denne inn fra fil, og skrives pa skjermen (startnummer, samt deltagernes nummer, navn og nasjon). NB: Om listen som leses fra fil ikke er sortert, sa gjores dette forst, og skrives sa til fil igjen. Statistikken for medaljer og poeng oppdateres da ogsa.
				grenbase.skrivResultatListe(grenNavn, tempOvelseID);	//	sender med grennavn og ovelse ID	//--path stemmer--funksjon ikke ferdig
				break;	
			case 'N':													//Om resultatliste ikke finnes, sa leses den fra tastaturet (ut fra deltagerlista), den sorteres, skrives til skjermen, skrives til fil og statistikkene oppdateres. Hvordan handtere deltagere som har brutt, ikke mott, blitt disket?
				grenbase.nyResultatListe(grenNavn, tempOvelseID);		//	sender med grennavn og ovelse ID	//--path stemmer--funksjon ikke ferdig
                //poengstatistikk.skrivTilFil();
				break;
			case 'F': 													//Om denne (filen) finnes sa slettes den. Men, husk forst a oppdatere statistikkene.
				grenbase.slettResultatListe(grenNavn, tempOvelseID);	//	sender med grennavn og ovelse ID	//--path stemmer--funksjon ikke ferdig
				break;
			}
			kommando = les("Resultat: Hva vil du gjore?");			    //leser kommando fra bruker
		}
	}
}

void skrivLitenMeny()		//	skriver meny om valgene tilgjengelig i hovedmeny switchen
{
	{
		std::cout << "\nFolgende kommandoer er tilgjengelig: \n";
		std::cout << "\n\t<N> Nasjon. ";
		std::cout << "\n\t<D> Deltager. ";
		std::cout << "\n\t<G> Gren. ";
		std::cout << "\n\t<O> Ovelse. ";
		std::cout << "\n\t<M> Medaljeoversikt. ";
		std::cout << "\n\t<P> Poengoversikt. ";
		std::cout << "\n\t<H> Full oversikt over mulige valg. ";
		std::cout << "\n\t<X> Avslutt. \n\n";
	}
}

void skrivMeny()			//	skriver ut hovedmenyen med alle valg og undervalg om brukeren benytter seg av <H> 
{
	std::cout << "\nFolgende kommandoer er tilgjengelig: \n";

	std::cout << "\n\t<N> Nasjon:   <N> - Registrer en ny nasjon. ";
	std::cout << "\n\t              <E> - Endre en nasjon. ";
	std::cout << "\n\t              <A> - Skriv hoveddataene om alle nasjoner. ";
	std::cout << "\n\t              <T> - Skriv en nasjons deltagertropp. ";
	std::cout << "\n\t              <S> - Skriv alle data om en gitt nasjon. ";
	std::cout << "\n\t			   <X> - Tilbake til hovedmeny. \n";

	std::cout << "\n\t<D> Deltager: <N> - Registrer en ny deltager. ";
	std::cout << "\n\t              <E> - Endre en deltager. ";
	std::cout << "\n\t              <A> - Skriv hoveddataene om alle deltagere. ";
	std::cout << "\n\t              <S> - Skriv alle data om en gitt deltager. ";
	std::cout << "\n\t			   <X> - Tilbake til hovedmeny. \n";

	std::cout << "\n\t<G> Gren:     <N> - Registrer en ny gren. ";
	std::cout << "\n\t              <E> - Endre en gren. ";
	std::cout << "\n\t              <A> - Skriv hoveddataene om alle grener. ";
	std::cout << "\n\t              <S> - Skriv alle data om en gitt gren. ";
	std::cout << "\n\t			   <X> - Tilbake til hovedmeny. \n";

	std::cout << "\n\t<O> Ovelse:   <Navn pa gren> - <N> - Registrer en ny ovelse. ";
	std::cout << "\n\t              <Navn pa gren> - <E> - Endre en ovelse. ";
	std::cout << "\n\t              <Navn pa gren> - <F> - Fjerne/slette en ovelse. ";
	std::cout << "\n\t              <Navn pa gren> - <A> - Skriv hoveddataene om alle ovelser. ";

	std::cout << "\n\t	      <Navn pa gren> - <L> - Lister:   <ovelse(id)> - <S> - Skriv deltager-/startliste. ";
	std::cout << "\n\t	                     		       <ovelse(id)> - <N> - Ny deltager-/startliste. ";
	std::cout << "\n\t	                     		       <ovelse(id)> - <E> - Endre deltager-/startliste. ";
	std::cout << "\n\t	                     		       <ovelse(id)> - <F> - Fjerne/slette deltager-/startliste. \n";

	std::cout << "\n\t	      <Navn pa gren> - <R> - Resultat: <ovelse(id)> - <S> - Skriv resultatliste. ";
	std::cout << "\n\t	  			 	       <ovelse(id)> - <N> - Ny resultatliste. ";
	std::cout << "\n\t	  			 	       <ovelse(id)> - <F> - Fjerne/slette resultatliste. \n";

	std::cout << "\n\t<M> Medaljeoversikt. \n";

	std::cout << "\n\t<P> Poengoversikt. \n";

	std::cout << "\n\t<H> Full oversikt over mulige valg. \n";

	std::cout << "\n\t<X> Avslutt. \n\n";
}
#pragma once

#include "ListTool2B.h"
#include "Nasjon.h"

//	Nasjoner-klassen, dens datamedlemmer og funksjonsheadinger
class Nasjoner
{
private:
	List* nasjonerList;

public:
	Nasjoner();	
	~Nasjoner();				
	void display();				//	skriver ut hoveddata om alle nasjoner
	void displayHoved();		//  displays nation with correct ID, 
	bool finnesID(char* ID);	//	returnerer true om nasjonen finnes
	void nyNasjon();			//	registrerer en ny nasjon
	void endreNasjon();			//	endrer en bestemt nasjon
	void nyDeltager(char* ID);	//	Legger til en i ant. deltager for aktuell nasjon
	void nasjonerLesFraFil();	//	leser nasjoner fra nasjon.dta fil
	void nasjonerSkrivTilFil();	//	skriver nasjoner til nasjoner.dta fil
};

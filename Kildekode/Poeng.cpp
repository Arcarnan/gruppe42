#pragma once		//erstatter "if !defined" --- "define"

//#if !defined (__Poeng_cpp)
//#define __Poeng_cpp

#include "Poeng.h"
#include "Deltagere.h"
#include "Medaljer.h"
#include <iostream>

extern Deltagere deltagerbase;
extern Medaljer medaljestatistikk;


//private:
//	List* poeng;


Poeng::Poeng() : Statistikk()
{
    antMedPoeng = 0;
    for (int i = 0; i < MAXNASJONER + 1; i++)
    {
        poeng[i] = 0;
    }
}

/*
void Poeng::lesFraFil()
{
antMedPoeng = 0;

std::ifstream inn("POENG.DTA");			//Ny innfil.
if (inn)
{
inn >> antMedPoeng;		inn.ignore();		//Hent poeng det skal v�re i datastrukturen.
for (int i = 1; i < antMedPoeng + 1; i++)
{
inn.ignore();
Statistikk::lesFraFil(inn);

inn >> poeng[i];
}
}
else
{
std::cout << "\nFant ikke POENG.DTA";
}
}
*/

void Poeng::skrivTilFil()
{
    std::ofstream ut("POENG.DTA");
    ut << antMedPoeng << "\n";

    for (int i = 1; i < antMedPoeng + 1; i++)		//Leser inn aller medaljene, gull, solv og bronse.
    {
        ut << "\n";
        Statistikk::skrivTilFil(ut, i);
        ut << "\n" << poeng[i];
    }

}


void Poeng::poengOversikt()
{
    for (int i = 1; i < antMedPoeng + 1; i++)		//Leser inn aller medaljene, gull, solv og bronse.
    {
        Statistikk::skrivNasjonForkortelse(i);
        std::cout << "\nPoeng: " << poeng[i];
    }
}

void Poeng::redigerResultater(int deltagere[MAXDELTAGERE], bool leggeTil)
{
    bool finnesNasjon;
    char tempNasjonId[NASJONIDLENGDE + 1];
    int deltagereMedPoeng;
    int plassNummer = 0;
    int fortegn = 0;
    if (leggeTil)
    {
        fortegn = 1;
    }
    else
    {
        fortegn = -1;
    }

    if (deltagere[0] > 5)
    {
        deltagereMedPoeng = 6;
    }
    else
    {
        deltagereMedPoeng = deltagere[0];
    }

    for (int i = deltagere[0]; i > deltagere[0] - deltagereMedPoeng; i--)
    {
        finnesNasjon = false;
        ++plassNummer;
        strcpy_s(tempNasjonId, NASJONIDLENGDE + 1, deltagerbase.getDeltagersNasjonsID(deltagere[i]));
        for (int g = 1; g < antallNasjoner + 1; g++)
        {
            if (!strcmp(tempNasjonId, nasjonsforkortelser[g]))
            {
                finnesNasjon = true;
            }
        }
        if (!finnesNasjon)
        {
            strcpy_s(nasjonsforkortelser[++antallNasjoner], NASJONIDLENGDE + 1, tempNasjonId);
        }

        for (int j = 1; j < antallNasjoner + 1; j++)
        {
            if (deltagere[i] != 0 && !strcmp(tempNasjonId, nasjonsforkortelser[j]))
            {
                if (!poeng[j])
                {
                    antMedPoeng++;
                }
                switch (plassNummer)
                {
                case 1:
                    poeng[j] += 7 * fortegn;
                    sorterLister();
                    medaljestatistikk.redigerMedalje(tempNasjonId, 0, leggeTil);
                    break;
                case 2:
                    poeng[j] += 5 * fortegn;
                    sorterLister();
                    medaljestatistikk.redigerMedalje(tempNasjonId, 1, leggeTil);
                    break;
                case 3:
                    poeng[j] += 4 * fortegn;
                    sorterLister();
                    medaljestatistikk.redigerMedalje(tempNasjonId, 2, leggeTil);
                    break;
                case 4:
                    poeng[j] += 3 * fortegn;
                    sorterLister();
                    break;
                case 5:
                    poeng[j] += 2 * fortegn;
                    sorterLister();
                    break;
                case 6:
                    poeng[j] += 1 * fortegn;
                    sorterLister();
                    break;
                default:
                    break;
                }
            }
        }
    }
    sorterLister();
}

void Poeng::sorterLister()
{
    medaljestatistikk.sorterLister(nasjonsforkortelser, poeng, antallNasjoner, antMedPoeng);
}
#pragma once

#include "konstanter.h"

char les(char* h);									//	Leser et upcaset tegn
//int les(char* h, int min, int max);					//	Leser et heltall mellom min og max
void les(char* h, char* t, int len = STRLEN);					//	Leser en ikke-blank streng
void lesID(char* tekst, char ID[NASJONIDLENGDE]);				//	Leser inn en ID, passer pa at den er uppercase og 3 lang
int les(char* tekst, int min, int max);	//	henter numerisk input mellom min og max
void oppdaterStatistikk(char poengNasjonsForkortelser[MAXNASJONER + 1][NASJONIDLENGDE + 1], int* poeng, int& poengsNasjoner, int& antallMedPoeng, char medaljeNasjonsForkortelser[MAXNASJONER + 1][NASJONIDLENGDE + 1], int medaljer[MAXNASJONER + 1][3], int& medaljersNasjoner, int& antallMedMedaljer);
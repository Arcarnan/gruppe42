#include <iostream>
#include <fstream>				//  ifstream, ofstream
#include <cstring>				//  strcpy, strlen, strncmp
#include <cstdio>
#include <algorithm>
#include "Ovelse.h"
#include "Hjelpefunksjoner.h"
#include "Deltagere.h"
#include "Poeng.h"

extern Deltagere deltagerbase;
extern Poeng poengstatistikk;

Ovelse::Ovelse(int ovlsID)
{
    char tempStr[STRLEN];
    char tempIdStr[5];
    int tempLen = 0;
    int tempTid = 0;

    ovelseID = ovlsID;
    sprintf_s(tempIdStr, "%d", ovelseID);
    tempLen = strlen(tempIdStr);
    std::cout << "\nOvelse med ID: " << ovelseID << " registreres. ";

    strcpy_s(deltagerlisteFilnavn, 11, "OV0000.STA");			//	setter navnet pa ovelsens deltagerliste
    for (int i = 0; i < tempLen; i++)
    {
        deltagerlisteFilnavn[6 - tempLen + i] = tempIdStr[i];
    }

    strcpy_s(resultatlisteFilnavn, 11, "OV0000.RES");			//	setter navnet pa ovelsens resultatliste
    for (int i = 0; i < tempLen; i++)
    {
        resultatlisteFilnavn[6 - tempLen + i] = tempIdStr[i];
    }

    les("Fullt/ reelt navn", tempStr);
    tempLen = strlen(tempStr) + 1;								//	Maler lengden med \0
    fulltNavn = new char[tempLen];								//	Allokerer minne
    strcpy_s(fulltNavn, tempLen, tempStr);						//	Kopierer navnet til pekeren

    for (int i = 0; i < TIDLEN; i++)
    {															//	leser inn tid i gyldige verdier
        switch (i)
        {
        case 0:
        {
            tempTid = les("Time", 0, 23);
            break;
        }
        case 2:
        {
            tempTid = les("Minutter", 0, 59);
            break;
        }
        }
        if (i % 2)
        {
            klokkeslett[i] = char((tempTid % 10) + 48);
        }
        else
        {
            klokkeslett[i] = char((tempTid / 10) + 48);
        }

    }
    klokkeslett[TIDLEN] = '\0';

    for (int i = 0; i < DATOLEN; i++)									//	Leser gyldige datoer, caster
    {																	//	som char og legger inn
        switch (i)														//	i arrayen
        {
        case 0:		tempTid = les("Dag", 1, 31); break;
        case 3:		tempTid = les("Maaned", 1, 12); break;
        case 6:		tempTid = les("Aarstall (siste to siffre)", 0, 99); break;
        }
        if (!(i % 3)) dato[i] = char((tempTid / 10) + 48);
        else if (i % 3 == 1) dato[i] = char((tempTid % 10) + 48);
        else dato[i] = '.';
    }
    dato[DATOLEN] = '\0';												//	Legger p� \0

    antallDeltagere = 0;												//	antall deltagere settes til 0
}

Ovelse::Ovelse(std::ifstream &inn, int antSifre)
{
    char tempStr[STRLEN];
    char tempIdStr[5];
    int tempLen;

    inn >> ovelseID;
    sprintf_s(tempIdStr, "%d", ovelseID);
    tempLen = strlen(tempIdStr);
    inn.ignore();

    strcpy_s(deltagerlisteFilnavn, 11, "OV0000.STA");			//	setter navnet pa ovelsens deltagerliste
    for (int i = 0; i < tempLen; i++)
    {
        deltagerlisteFilnavn[6 - tempLen + i] = tempIdStr[i];
    }

    strcpy_s(resultatlisteFilnavn, 11, "OV0000.RES");			//	setter navnet pa ovelsens resultatliste
    for (int i = 0; i < tempLen; i++)
    {
        resultatlisteFilnavn[6 - tempLen + i] = tempIdStr[i];
    }

    inn.getline(tempStr, STRLEN);
    tempLen = strlen(tempStr) + 1;									//	Maler lengde med \0
    fulltNavn = new char[tempLen];									//	Allokerer minne
    strcpy_s(fulltNavn, tempLen, tempStr);							//	Kopierer navnet til pekeren

    inn.getline(dato, STRLEN);

    inn.getline(klokkeslett, STRLEN);

    antallDeltagere = 0;											//	antall deltagere lagres ikke pa ovelsens fil

    redigerPoeng(antSifre, true);
}

int Ovelse::hentOvelseID()											// returnerer ovelsens ID
{
    return ovelseID;
}

const char* Ovelse::hentOvelseNavn()								// returnerer ovelsens navn
{
    return fulltNavn;
}

void Ovelse::endreOvelseData()
{
    char kommando;
    char tempStr[STRLEN];								//	Midlertidig char array
    int tempLen = 0;									//	Char array'ens lengde
    int tempDato = 0;

    std::cout << "\n\t\tFolgende informasjon kan endres: \n";
    std::cout << "\n\t<N> - Ovelsens navn. ";
    std::cout << "\n\t<K> - Klokkeslett. ";
    std::cout << "\n\t<D> - Dato. ";
    std::cout << "\n\t<X> - Avslutt endring. ";

    kommando = les("\nOvelse: Hva vil du endre?");		//leser kommando fra bruker

    while (kommando != 'X')
    {
        switch (kommando)
        {
        case 'N':
            delete[] fulltNavn;
            les("Ovelsens nye navn", tempStr);			//	Leser nasjonens navn
            tempLen = strlen(tempStr) + 1;				//	Maler lengden med \0
            fulltNavn = new char[tempLen];				//	Allokerer minne
            strcpy_s(fulltNavn, tempLen, tempStr);		//	Kopierer navnet til pekeren
            break;

        case 'K':
        {
            int tempTid = 0;
            for (int i = 0; i < TIDLEN; i++)			//	leser inn tid i gyldige verdier
            {
                switch (i)
                {
                case 0:
                {
                    tempTid = les("Time", 0, 23);
                    break;
                }
                case 2:
                {
                    tempTid = les("Minutter", 0, 59);
                    break;
                }
                }
                if (i % 2)
                {
                    klokkeslett[i] = char((tempTid % 10) + 48);
                }
                else
                {
                    klokkeslett[i] = char((tempTid / 10) + 48);
                }

            }
            klokkeslett[TIDLEN] = '\0';
            break;
        }
        case 'D':
            for (int i = 0; i < DATOLEN; i++)			//	Leser gyldige datoer, caster
            {											//	som char og legger inn
                switch (i)								//	i arrayen
                {
                case 0:		tempDato = les("Dag", 1, 31); break;
                case 3:		tempDato = les("Maaned", 1, 12); break;
                case 6:		tempDato = les("Aarstall (siste to siffre)", 0, 99); break;
                }
                if (!(i % 3)) dato[i] = char((tempDato / 10) + 48);
                else if (i % 3 == 1) dato[i] = char((tempDato % 10) + 48);
                else dato[i] = '.';
            }
            dato[DATOLEN] = '\0';							//	Legger p� \0
            break;

        default:
            std::cout << "\nDette er desverre ikke et gyldig valg. \n";
            break;
        }
        std::cout << "\nOvelse under endring: " << ovelseID << ", " << fulltNavn << ". \n";
        kommando = les("Ovelse: Hva vil du endre?");						     //leser kommando fra bruker
    }
}

void Ovelse::display()
{
    std::cout << "\nOvelse: " << ovelseID << ", " << fulltNavn << ". ";
    std::cout << "\n\tDato: " << dato << ". ";
    std::cout << "\n\tKlokkeslett: " << klokkeslett[0] << klokkeslett[1] << ":" << klokkeslett[2] << klokkeslett[3] << ". ";
    std::cout << "\n\tAntall deltagere: " << antallDeltagere << ". \n";
}

void Ovelse::ovelseSkrivTilFil(std::ofstream & ut)
{
    ut << ovelseID << "\n"
        << fulltNavn << "\n"
        << dato << "\n"
        << klokkeslett << "\n";
}

void Ovelse::skrivResultatListe(int antallSiffre)
{
    int deltagerliste[MAXDELTAGERE];
    int resultatliste[MAXDELTAGERE];

    lesListerFraFil(deltagerliste, resultatliste);
    sorterLister(deltagerliste, resultatliste, antallSiffre);

    if (deltagerliste[0] && resultatliste[0])		//	Hvis begge ble lest inn ordentlig og ingen har tukla med filene
    {
        for (int i = 1; i <= deltagerliste[0]; i++)
        {
            deltagerbase.skrivDeltagerFraOvelse(deltagerliste[i]);
            if (antallSiffre < 3)
            {
                std::cout << "\n\tPoeng:            " << resultatliste[i] << '\n';
            }
            else
            {
                std::cout << "\n\tTid:              ";
                std::cout << (resultatliste[i] / (10 ^ (antallSiffre - 2)));
                std::cout << ':' << (resultatliste[i] / (10 ^ (antallSiffre - 4))) % 100;
                std::cout << ':' << resultatliste[i] % 10000 << '\n';
            }
        }
    }
    else
    {
        std::cout << "\nEn eller flere filer ble ikke lest korrekt. Avbryter handling.\n";
    }
}

void Ovelse::nyResultatListe(int antallSiffre)
{
    int deltagerliste[MAXDELTAGERE];
    int resultatliste[MAXDELTAGERE];
    int temp = 0;
    char tempStr[13];

    lesListerFraFil(deltagerliste, resultatliste);

    if (deltagerliste[0] && !resultatliste[0])
    {
        resultatliste[0] = antallDeltagere;

        for (int i = 1; i <= antallDeltagere; i++)
        {
            std::cout << "\nStartnummer:      " << i;
            deltagerbase.skrivDeltagerFraOvelse(deltagerliste[i]);

            if (antallSiffre < 3)
            {
                resultatliste[i] = les("Poeng", 0, (int(pow(10, antallSiffre)) - 1));
            }
            else
            {
                temp = les("Minutter", 0, 59);
                resultatliste[i] = temp * (10 ^ (antallSiffre - 2));
                temp = les("Sekunder", 0, 59);
                resultatliste[i] += temp * (10 ^ (antallSiffre - 4));

                switch (antallSiffre)
                {
                case 5:		strcpy_s(tempStr, 13, "Tideler"); break;
                case 6:		strcpy_s(tempStr, 13, "Hundredeler"); break;
                case 7:		strcpy_s(tempStr, 13, "Tusendeler"); break;
                }
                temp = les(tempStr, 0, ((10 ^ (antallSiffre - 4)) - 1));
                resultatliste[i] += temp;
            }
        }
        sorterLister(deltagerliste, resultatliste, antallSiffre);
        //	Oppdatere medalje- og poengstatistikk
        skrivListerTilFil(deltagerliste, resultatliste);
        redigerPoeng(antallSiffre, true);
    }
    else
    {
        std::cout << "\nFeil: handlingen krever at deltagerliste finnes,"
            << "\nog at resultatliste ikke finnes.\n";
    }
}

void Ovelse::slettResultatliste(int antallSiffre)
{
    int deltagerliste[MAXDELTAGERE];
    int resultatliste[MAXDELTAGERE];
    char kommando;

    lesListerFraFil(deltagerliste, resultatliste);

    if (deltagerliste[0] && resultatliste[0])
    {
        //	------trenger her en funksjon som opdaterer statistikken
        //	------for brukeren blir spurt om filen virkelig onskes slettet

        //	Husk at statistikken ikke skal oppdateres hvis filen ikke slettes!



        std::cout << "\nEr du sikker pa at du vil slette filen " << resultatlisteFilnavn << "? ";
        kommando = les("<Y> / <N>");

        if (kommando == 'Y')								//	passer pa a ikke slette filen bare fordi brukeren trykker feil
        {
            redigerPoeng(antallSiffre, false);

            if (std::remove(resultatlisteFilnavn) != 0)		//	sletter filen
            {
                std::perror("Det oppsto et problem under sletting av filen. ");
            }
            else
            {
                std::cout << ("Filen er na slettet. ");
            }
        }
        else
        {
            std::cout << "\nOpperasjon avbrudd. " << resultatlisteFilnavn << " er IKKE slettet. ";
        }
    }
    else
    {
        std::cout << "\nFeil: handlingen krever at deltagerliste og resultatliste finnes.\n";
    }
}

Ovelse::~Ovelse()
{
    delete[] fulltNavn;
}

void Ovelse::skrivDeltagerStartliste()
{
    int deltagerArray[MAXDELTAGERE];
    int resultatArray[MAXDELTAGERE];

    lesListerFraFil(deltagerArray, resultatArray);

    if (deltagerArray[0])											//	hvis deltagerliste er funnet
    {
        antallDeltagere = deltagerArray[0];							//	forste plass pa fil er antall deltagere

        std::cout << "\nStartliste: ";
        for (int i = 1; i <= antallDeltagere; i++)
        {
            deltagerbase.skrivDeltagerFraOvelse(deltagerArray[i]);	//	skriver aktuell data for deltager
        }
    }
}

void Ovelse::nyDeltagerStartliste()
{
    int tempDeltager;
    int deltagerliste[MAXDELTAGERE];
    int resultatliste[MAXDELTAGERE];
    bool finnesIStartliste;

    lesListerFraFil(deltagerliste, resultatliste);

    if (deltagerliste[0])											//	hvis deltagerliste er funnet
    {
        std::cout << "\nFilen '" << deltagerlisteFilnavn << "' eksisterer allerede. ";
    }
    else
    {
        antallDeltagere = deltagerliste[0] = les("\nAntall deltagere: ", MINDELTAGERE, MAXDELTAGERE);

        for (int i = 1; i <= antallDeltagere;)							//	leser inn deltagere til ny liste
        {
            tempDeltager = les("\nDeltagernummer (ID): ", MINDELTAGERNUMMER, MAXDELTAGERNUMMER);

            if (deltagerbase.finnesDeltagerID(tempDeltager))			//	hvis deltager ID eksisterer
            {
                finnesIStartliste = false;

                for (int j = 1; j <= antallDeltagere; j++)
                {
                    if (deltagerliste[j] == tempDeltager)
                    {
                        std::cout << "\nDenne deltageren er allerede med i denne startlisten. ";
                        finnesIStartliste = true;
                    }
                }
                if (!finnesIStartliste)									// hvis deltager ID ikke allerede er i startlisten
                {
                    deltagerliste[i] = tempDeltager;
                    i++;
                }
            }
            else
            {
                std::cout << "\nDette deltagernummeret eksisterer ikke. ";
            }
        }
        skrivListerTilFil(deltagerliste, resultatliste);
    }
}

void Ovelse::endreDeltagerStartliste()
{
    int tempDeltager;
    char kommando;
    int deltagerliste[MAXDELTAGERE];
    int resultatliste[MAXDELTAGERE];
    bool finnesIStartliste;

    lesListerFraFil(deltagerliste, resultatliste);

    if (!resultatliste[0] && deltagerliste[0])									//	om resultatfil ikke finnes og deltagerliste finnes
    {
        antallDeltagere = deltagerliste[0];
        std::cout << "\nEksisterende deltagerliste: ";

        for (int i = 1; i <= antallDeltagere; i++)								//	skriver ut eksisterende deltagerliste
        {
            std::cout << "\nStartnummer: " << i << ", ";
            deltagerbase.skrivDeltagerFraOvelse(deltagerliste[i]);
        }

        std::cout << "\n\n\t\tFolgende underkommandoer er tilgjengelige for endring av deltagerliste: \n";
        std::cout << "\n\t<N> - Legg til en ny deltager. ";
        std::cout << "\n\t<S> - Slett en deltager. \n";
        std::cout << "\n\t<X> - Tilbake til deltagerliste. \n";

        kommando = les("Deltagerliste: Legg til eller fjern deltager?");		//	leser kommando fra bruker

        while (kommando != 'X')
        {
            switch (kommando)
            {
            case 'N':
                tempDeltager = les("\nDeltagernummer (ID): ", MINDELTAGERNUMMER, MAXDELTAGERNUMMER);

                if (deltagerbase.finnesDeltagerID(tempDeltager))			//	hvis deltager ID eksisterer
                {
                    finnesIStartliste = false;

                    for (int j = 1; j <= antallDeltagere; j++)
                    {
                        if (deltagerliste[j] == tempDeltager)
                        {
                            std::cout << "\nDenne deltageren er allerede med i denne startlisten. ";
                            finnesIStartliste = true;
                        }
                    }
                    if (!finnesIStartliste)									// hvis deltager ID ikke allerede er i startlisten
                    {
                        deltagerliste[++antallDeltagere] = tempDeltager;
                        deltagerliste[0] = antallDeltagere;
                    }
                }
                else
                {
                    std::cout << "\nDette deltagernummeret eksisterer ikke. ";
                }
                break;
            case 'S':
                tempDeltager = les("\nDeltagernummer: ", MINDELTAGERNUMMER, MAXDELTAGERNUMMER);

                for (int i = 1; i <= antallDeltagere; i++)
                {
                    finnesIStartliste = false;

                    if (deltagerliste[i] == tempDeltager)						//	hvis deltagernummeret eksisterer i liste
                    {
                        finnesIStartliste = true;

                        std::cout << "\nSletter deltager: " << i << ", ";		//	skriver data for deltager som blir slettet
                        deltagerbase.skrivDeltagerFraOvelse(deltagerliste[i]);

                        for (int j = i; j < antallDeltagere; j++)				//	ikke <=, fordi det blir en mindre deltager
                        {
                            deltagerliste[j] = deltagerliste[j + 1];			//	skrive alle ett hakk til hoyre fra den vi sletter
                        }
                        deltagerliste[0] = --antallDeltagere;										//	minsker antall deltagere med 1
                        break;
                    }
                }
                if (!finnesIStartliste)
                {
                    std::cout << "\nDette deltagernummeret finnes ikke i startlisten. ";
                }
                break;
            default:
                std::cout << "\nDette er desverre ikke et gyldig valg. \n";
                break;
            }skrivListerTilFil(deltagerliste, resultatliste);
            kommando = les("Deltagerliste: Legg til eller fjern deltager?");	//	leser kommando fra bruker
        }
        skrivListerTilFil(deltagerliste, resultatliste);
    }
    else
    {
        std::cout << "\nKan ikke endre data for en deltagerliste som har en resultatliste, eller om deltagerliste ikke eksisterer. ";
    }
}

void Ovelse::slettDeltagerStartliste()
{
    int deltagerliste[MAXDELTAGERE];
    int resultatliste[MAXDELTAGERE];
    char kommando;

    lesListerFraFil(deltagerliste, resultatliste);

    if (!resultatliste[0] && deltagerliste[0])		//	om resultatfil ikke finnes og deltagerliste finnes
    {
        std::cout << "\nEr du sikker pa at du vil slette filen " << deltagerlisteFilnavn << "? ";
        kommando = les("<Y> / <N>");

        if (kommando == 'Y')		//	passer pa a ikke slette filen bare fordi brukeren trykker feil
        {
            if (std::remove(deltagerlisteFilnavn) != 0)
            {
                std::perror("Det oppsto et problem under sletting av filen");
            }
            else
            {
                std::cout << ("Filen er na slettet. ");
                antallDeltagere = 0;
            }
        }
        else
        {
            std::cout << "\nOpperasjon avbrutt. " << deltagerlisteFilnavn << " er IKKE slettet. ";
        }
    }
    else
    {
        std::cout << "\nKan ikke slette en startliste som ikke eksisterer, eller som har en resultatliste. ";
    }
}

void Ovelse::sorterLister(int* deltagere, int* resultat, int antallSiffre)
{
    //	Skal bare sjekke hvordan vi gjorde dette igjen
    //	Hvis antallSiffre er mindre enn 3, saa sorterer vi fra storst til minst.
    //	Ellers sorterer vi fra minst til storst.
    int antallUsorterte = antallDeltagere;
    int tempIndeks = 0;
    int tempVerdi = 0;

    while (antallUsorterte)
    {
        tempIndeks = 0;
        if (antallSiffre < 3)
        {
            tempVerdi = 0;
        }
        else
        {
            tempVerdi = INT_MAX;
        }

        for (int i = 1; i <= antallUsorterte; i++)
        {
            if (antallSiffre < 3)			//	Sorteres paa poeng
            {
                if (resultat[i] > tempVerdi)	//	Finn den med storst poengsum
                {
                    tempIndeks = i;             //  Lagre dens indeks
                    tempVerdi = resultat[i];    //  Oppdatere hittil storste poengsum
                }
            }
            else							//	Sorteres paa tid
            {
                if (resultat[i] < tempVerdi)	//	Finn den med minst tid
                {
                    tempIndeks = i;             //  Lagre dens indeks
                    tempVerdi = resultat[i];    //  Oppdatere hittil korteste tid
                }
            }
        }
        std::swap(resultat[tempIndeks], resultat[antallUsorterte]);
        std::swap(deltagere[tempIndeks], deltagere[antallUsorterte]);
        --antallUsorterte;
    }
}

void Ovelse::lesListerFraFil(int* deltagere, int* resultat)
{
    std::ifstream innfil;
    innfil.open(deltagerlisteFilnavn);

    if (innfil)
    {
        innfil >> deltagere[0]; innfil.ignore(2);		//	leser inn antall deltagere i listen
        antallDeltagere = deltagere[0];

        for (int i = 1; i <= deltagere[0]; i++)
        {
            innfil >> deltagere[i];
            innfil.ignore();
        }
    }
    else
    {
        std::cout << "\nKunne ikke finne deltagerfilen.\n";
        deltagere[0] = 0;
    }

    innfil.close();
    innfil.open(resultatlisteFilnavn);

    if (innfil)
    {
        innfil >> resultat[0]; innfil.ignore(2);        //	leser inn antall deltagere i listen
        if (resultat[0] == deltagere[0])
        {
            for (int i = 1; i <= resultat[0]; i++)
            {
                innfil >> resultat[i];
                innfil.ignore();
            }
        }
        else
        {
            std::cout << "\n*FATAL FEIL* Antall deltagere i \'" << deltagerlisteFilnavn << "\' er";
            std::cout << "\n\tikke likt antall deltagere i \'" << resultatlisteFilnavn << "\'.";
            std::cout << "\n\tVennligst avslutt programmet og kontroller filene.\n";
        }
    }
    else
    {
        std::cout << "\nKunne ikke finne resultatfilen.\n";
        resultat[0] = 0;
    }
    innfil.close();
}

void Ovelse::skrivListerTilFil(int* deltagere, int* resultat)
{
    std::ofstream utfil;
    utfil.open(deltagerlisteFilnavn);

    if (utfil)
    {
        utfil << deltagere[0] << "\n\n";

        for (int i = 1; i <= deltagere[0]; i++)
        {
            utfil << deltagere[i] << '\n';
        }
    }
    else
    {
        std::cout << "\nDet oppsto en feil naar programmet skrev til \'" << deltagerlisteFilnavn << "\'.\n";
    }

    utfil.close();
    if (resultat[0])
    {
        utfil.open(resultatlisteFilnavn);
        if (utfil)
        {
            utfil << resultat[0] << "\n\n";

            for (int i = 1; i <= resultat[0]; i++)
            {
                utfil << resultat[i] << '\n';
            }
        }
        else
        {
            std::cout << "\nDet oppsto en feil naar programmet skrev til \'" << resultatlisteFilnavn << "\'.\n";
        }
        utfil.close();
    }
}


void Ovelse::redigerPoeng(int antSifre, bool leggeTil)
{
    int deltagerliste[MAXDELTAGERE];
    int resultatliste[MAXDELTAGERE];

    lesListerFraFil(deltagerliste, resultatliste);
    if (resultatliste[0])
    {
        sorterLister(deltagerliste, resultatliste, antSifre);
        poengstatistikk.redigerResultater(deltagerliste, leggeTil);
        skrivListerTilFil(deltagerliste, resultatliste);
    }
}
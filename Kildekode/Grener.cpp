#pragma once

#include <iostream>				//  cin, cout
#include <cstring>				//  strcpy, strlen, strncmp
#include <fstream>
#include "Gren.h"
#include "konstanter.h"
#include "Hjelpefunksjoner.h"
#include "Grener.h"
extern Grener grenbase;

//	Grener-klassens funksjonsinnmat

Grener::Grener()
{
    grenerList = new List(Sorted);
}

Grener::~Grener()
{
    delete grenerList;
}

bool Grener::finnesGren(char* grenNvn)						//	sjekker om grenen finnes
{
    Gren* grenPtr = nullptr;
    int antallGrener = grenerList->noOfElements();

    for (int i = 1; i <= antallGrener; i++)
    {
        grenPtr = (Gren*)grenerList->removeNo(i);
        grenerList->add(grenPtr);

        if (grenerList->inList(grenNvn))					// dersom grenen finnes
        {
            return true;
        }
    }
    return false;
}

bool Grener::finnesOvelse(char* grenNvn, int tempID)		//	mellomledd for a sjekke om en ovelse eksisterer
{
    Gren* grenPtr = nullptr;

    grenPtr = (Gren*)grenerList->remove(grenNvn);
    grenerList->add(grenPtr);

    return (grenPtr->finnesOvelse(tempID));
}

void Grener::nyGren()
{
    Gren* grenPtr = nullptr;
    char grenNavn[STRLEN];

    les("Gren navn", grenNavn);

    if (finnesGren(grenNavn))								//	dersom grennavnet eksisterer
    {
        std::cout << "\nDenne grenen finnes allerede i systemet. ";
    }
    else
    {
        grenPtr = new Gren(grenNavn);
        grenerList->add(grenPtr);
    }
}

void Grener::display()
{
    Gren* grenPtr = nullptr;
    char grenNavn[STRLEN];

    les("Gren navn", grenNavn);
    grenPtr = (Gren*)grenerList->remove(grenNavn);

    if (grenPtr)				//	true hvis grennavn finnes
    {
        grenPtr->display();
        grenerList->add(grenPtr);
    }
    else
    {
        std::cout << "\nDenne grenen finnes ikke i systemet. ";
    }

}

void Grener::displayHoved()		//	viser hoveddata for grener
{
    Gren* grenPtr = nullptr;
    int antallGrener = grenerList->noOfElements();

    for (int i = 1; i <= antallGrener; i++)
    {
        grenPtr = (Gren*)grenerList->removeNo(i);
        grenPtr->displayHoved();
        grenerList->add(grenPtr);
    }
}

void Grener::displayOvelser(char* grenNvn)
{
    Gren* grenPtr = nullptr;
    grenPtr = (Gren*)grenerList->remove(grenNvn);

    if (grenPtr)				//	viser alle data for en spesifik gren
    {
        grenPtr->displayOvelser();
        grenerList->add(grenPtr);
    }
    else
    {
        std::cout << "\nDenne grenen finnes ikke i systemet. ";
    }
}

void Grener::endreGren()
{
    Gren* grenPtr = nullptr;
    char grenNavn[STRLEN];

    les("Gren navn", grenNavn);
    grenPtr = (Gren*)grenerList->remove(grenNavn);

    if (grenPtr)					//	dersom grenen eksisterer
    {
        grenPtr->endreData();
        grenerList->add(grenPtr);
    }
    else
    {
        std::cout << "\nDenne grenen finnes ikke i systemet. ";
    }
}

void Grener::nyOvelse(char* grenNavn)
{
    bool finnesOvelseID;
    int tempID;
    int antallGrener = grenerList->noOfElements();
    Gren* grenPtr = nullptr;

    do
    {
    tempID = les("Unik ovelse ID", MINOVELSER, MAXOVELSEID);
        finnesOvelseID = false;
        for (int i = 1; i <= antallGrener; i++)
        {
            grenPtr = (Gren*)grenerList->removeNo(i);
            grenerList->add(grenPtr);

            if (grenPtr->finnesAllerede(tempID) != 0)
            {
                finnesOvelseID = true;
                std::cout << "\nDenne ovelses IDen finnes allerede. \n";
            }
        }
    } while (finnesOvelseID);

    grenPtr = (Gren*)grenerList->remove(grenNavn);
    grenerList->add(grenPtr);

    if (grenPtr)					//	dersom grenen eksisterer
    {
        grenPtr->nyOvelse(tempID);
    }
    else
    {
        std::cout << "\nDenne grenen finnes ikke i systemet. ";
    }
}

void Grener::endreOvelse(char* grenNavn)
{
    Gren* grenPtr = nullptr;
    grenPtr = (Gren*)grenerList->remove(grenNavn);

    if (grenPtr)					//	dersom grenen eksisterer
    {
        grenPtr->endreOvelse();
        grenerList->add(grenPtr);
    }
    else
    {
        std::cout << "\nDenne grenen finnes ikke i systemet. ";
    }
}

void Grener::grenerLesFraFil()
{
    int antallGrener;
    char grenNavn[STRLEN];
    grenerList = new List(Sorted);
    Gren* grenPtr = nullptr;

    std::ifstream inn("GRENER.DTA");

    inn >> antallGrener;
    inn.ignore();

    if (inn)
    {
        for (int i = 1; i <= antallGrener; i++)
        {
            inn.ignore();
            inn.getline(grenNavn, STRLEN);
            grenPtr = new Gren(inn, grenNavn);
            grenerList->add(grenPtr);
        }
    }
    else
    {
        std::cout << "\nKunne ikke finne filen 'GRENER.DTA'. ";
    }
}

void Grener::grenerSkrivTilFil()
{
    Gren* grenPtr = nullptr;
    int antallGrener = grenerList->noOfElements();

    std::ofstream ut;
    ut.open("GRENER.DTA");

    if (ut)
    {
        ut << antallGrener << "\n";

        for (int i = 1; i <= antallGrener; i++)
        {
            grenPtr = (Gren*)grenerList->removeNo(i);
            grenPtr->grenSkrivTilFil(ut);
            grenerList->add(grenPtr);
        }
    }
    else
    {
        std::cout << "\nKunne ikke finne filen 'GRENER.DTA'. ";
    }
}

void Grener::slettOvelse(char* grenNavn)
{
    Gren* grenPtr = nullptr;
    grenPtr = (Gren*)grenerList->remove(grenNavn);

    if (grenPtr)					//	dersom grenen eksisterer
    {
        grenPtr->slettOvelse();
        grenerList->add(grenPtr);
    }
    else
    {
        std::cout << "\nDenne grenen finnes ikke i systemet. ";
    }
}

void Grener::skrivResultatListe(char* grenNavn, int ovelseId)
{
    Gren* grenPtr = (Gren*)grenerList->remove(grenNavn);
    grenPtr->skrivResultatListe(ovelseId);
    grenerList->add(grenPtr);
}

void Grener::nyResultatListe(char* grenNavn, int ovelseId)
{
    Gren* grenPtr = (Gren*)grenerList->remove(grenNavn);
    grenPtr->nyResultatListe(ovelseId);
    grenerList->add(grenPtr);
}

void Grener::slettResultatListe(char* grenNavn, int ovelseId)
{
    Gren* grenPtr = (Gren*)grenerList->remove(grenNavn);
    grenPtr->slettResultatliste(ovelseId);
    grenerList->add(grenPtr);
}

void Grener::skrivDeltagerStartliste(char* grenNvn, int tempID)
{
    Gren* grenPtr = (Gren*)grenerList->remove(grenNvn);
    grenPtr->skrivDeltagerStartliste(tempID);
    grenerList->add(grenPtr);
}

void Grener::nyDeltagerStartliste(char* grenNvn, int tempID)
{
    Gren* grenPtr = (Gren*)grenerList->remove(grenNvn);
    grenPtr->nyDeltagerStartliste(tempID);
    grenerList->add(grenPtr);
}

void Grener::endreDeltagerStartliste(char* grenNvn, int tempID)
{
    Gren* grenPtr = (Gren*)grenerList->remove(grenNvn);
    grenPtr->endreDeltagerStartliste(tempID);
    grenerList->add(grenPtr);
}

void Grener::slettDeltagerStartliste(char* grenNvn, int tempID)
{
    Gren* grenPtr = (Gren*)grenerList->remove(grenNvn);
    grenPtr->slettDeltagerStartliste(tempID);
    grenerList->add(grenPtr);
}

/*
FILFORMAT:
Antall Grener

Gren navn
type maaling
antall sifre i maalingen
antall ovelser
ovelse1 - ovelsens ID
ovelse1 - ovelsens fulle navn
ovelse1 - dato for ovelsen
ovelse1 - klokkeslett for ovelsen
ovelse1 - antall deltagere
ovelse2 - ovelsens ID
ovelse2 - ovelsens fulle navn
ovelse2 - dato for ovelsen
ovelse2 - klokkeslett for ovelsen
ovelse2 - antall deltagere

Gren navn
type maaling
antall sifre i maalingen
antall ovelser
*/

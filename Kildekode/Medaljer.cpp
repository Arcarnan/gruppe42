#include <fstream>
#include <iostream>
#include "Medaljer.h"
#include "Hjelpefunksjoner.h"

Medaljer::Medaljer() : Statistikk()
{
    antallNasjonerMedMedaljer = 0;
    for (int i = 1; i <= MAXNASJONER; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            medaljer[i][j] = 0;
        }
    }
}

/*
void Medaljer::lesFraFil()
{
std::ifstream inn("MEDALJER.DTA");			//Ny innfil.
if (inn)
{
inn >> antallNasjonerMedMedaljer;		inn.ignore();		//Hent antall medaljer det skal v�re i datastrukturen.
for (int i = 1; i < antallNasjonerMedMedaljer + 1; i++)		//Leser inn aller medaljene, gull, solv og bronse.
{
inn.ignore();
Statistikk::lesFraFil(inn);
for (int j = 0; j < 3; j++)
{
inn >> medaljer[j][i];
}
}
}
else
{
std::cout << "\nFant ikke MEDALJER.DTA";
}
}
*/

void Medaljer::skrivTilFil()
{
    std::ofstream ut("MEDALJER.DTA");
    ut << antallNasjonerMedMedaljer << "\n";

    for (int i = 1; i < antallNasjonerMedMedaljer + 1; i++)		//Leser inn aller medaljene, gull, solv og bronse.
    {
        ut << "\n";
        Statistikk::skrivTilFil(ut, i);
        ut << "\n";
        for (int j = 0; j < 3; j++)
        {
            ut << medaljer[i][j];
            if (j < 3 - 1) ut << " ";					//Sorger for at det ikke er space p� slutten av linjen.
        }
    }
}

void Medaljer::medaljeOversikt()
{
    for (int i = 1; i < antallNasjonerMedMedaljer + 1; i++)		//Leser inn aller medaljene, gull, solv og bronse.
    {
        if (medaljer[i][0] || medaljer[i][1] || medaljer[i][2])
        {
            Statistikk::skrivNasjonForkortelse(i);
            std::cout << "\tGull: " << medaljer[i][0] << "\tSolv: " << medaljer[i][1] << "\tBronse: " << medaljer[i][2];
        }
    }
}

void Medaljer::sorterLister(char poengNasjonsForkortelser[MAXNASJONER + 1][NASJONIDLENGDE + 1], int* poeng, int& poengsNasjoner, int& antallMedPoeng)
{
    oppdaterStatistikk(poengNasjonsForkortelser, poeng, poengsNasjoner, antallMedPoeng, nasjonsforkortelser, medaljer, antallNasjoner, antallNasjonerMedMedaljer);
}

void Medaljer::redigerMedalje(char* nasjonsID, int medalje, bool skalHa)
{


    for (int i = 1; i <= antallNasjonerMedMedaljer; i++)
    {
        if (!strcmp(nasjonsforkortelser[i], nasjonsID))
        {
            if (skalHa)
            {
                medaljer[i][medalje]++;
            }
            else
            {
                medaljer[i][medalje]--;
            }
        }
    }
}
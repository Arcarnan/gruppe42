#pragma once

#include "ListTool2B.h"
#include "Ovelse.h"

//	Gren-klassen, dens datamedlemmer og funksjonsheadinger

class Gren : public TextElement
{
private:
	char typeMaaling;							//	hvordan prestasjon for gren lagres, [T]id eller [P]oeng
	int antOvelser;								//	antall ovelser i grenen
	int antSifre;								// antall sifre i poeng
	Ovelse* ovelsePtr[MAXOVELSERPERGREN + 1];	//	array med pekere til ovelseobjekter
public:
	Gren();										//	tom constructor
	Gren(std::ifstream & inn, char* grenNvn);	//	constructor, leser fra fil
	Gren(char* grenNvn);						//	constructor med tilsendt grennavn
	~Gren();									//	destructor
	void display();								//	viser hoveddata for gren og alle ovelser
	void displayHoved();						//	viser hoveddata for en gren
	void displayOvelser();						//	viser data for alle ovelser
	void endreData();							//	endre grens data
	void grenSkrivTilFil(std::ofstream & ut);	//	skriver grenens data til fil
	void nyOvelse(int tempOvelseID);							//	registrerer ny ovelse
	void endreOvelse();							//	endre data for en ovelse
	int finnesAllerede(int ovlsID);				//	returnerer ovelsens plass i listen om den finnes
	void endreOvelseData();						//	endre data for en ovelse
	void slettOvelse();							//	slette en ovelse
	bool finnesOvelse(int ovelseID);			//	sjekker om ovelse finnes
	void skrivResultatListe(int ovelseId);		//	viser resultatliste for en ovelse pa skjerm
	void nyResultatListe(int ovelsesId);		//	registrerer en ny resultatliste for ovelse
	void slettResultatliste(int ovelsesId);		//	sletter resultatlisten for en ovelse
	void skrivDeltagerStartliste(int ovlsID);	//	viser deltagerliste for en ovelse
	void nyDeltagerStartliste(int ovlsID);		//	registrerer ny deltagerliste for en ovelse
	void endreDeltagerStartliste(int ovlsID);	//	endre deltagerliste for en ovelse
	void slettDeltagerStartliste(int ovlsID);	//	slett deltagerliste for en ovelse
};

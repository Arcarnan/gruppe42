#pragma once

#include "ListTool2B.h"
#include "Deltager.h"

//	Deltager-klassen, dens datamedlemmer og funksjonsheadinger

class Deltagere
{
private:
	List* deltagerListe;

	Deltager* finnDeltager();		//	Satt denne private, slik at ingen utenfor objektet kan fjerne fra listen og faa pekere til deltagere
public:
	Deltagere();
	~Deltagere();
	void leggTilDeltager();
	bool finnesNavn(char* id);
	void skrivHoveddata();
	void skrivDeltager();
	void skrivDeltagerFraOvelse(int id);
	void endreEnDeltager();
	virtual void display();
	void displayNasjonTropp();
	void skrivTilFil();
	void lesFraFil();
	int finnesDeltagerID(int deltagerID);
    const char* getDeltagersNasjonsID(int deltagerID);
};
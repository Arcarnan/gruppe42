#pragma once

#include "ListTool2B.h"			//  List tool

void valgN();					//	nasjon				
void valgD();					//	deltager				
void valgG();					//	gren					
void valgO();					//	ovelse		
void valgL(char* grenNavn);		//	lister   	
void valgR(char* grenNavn);		//	resultat	
void skrivLitenMeny();			//	Skriver kortfattet meny
void skrivMeny();				//	Skriver full meny

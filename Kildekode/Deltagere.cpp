#include "Deltagere.h"
#include "Nasjoner.h"
#include "Hjelpefunksjoner.h"
#include "Konstanter.h"
#include <iostream>
#include <fstream>

extern Deltagere deltagerbase;
extern Nasjoner nasjonbase;


//	Deltagere-klassens funksjonsinnmat
Deltagere::Deltagere()
{
	deltagerListe = new List(Sorted);
}

Deltagere::~Deltagere()
{
	delete deltagerListe;
}

void Deltagere::leggTilDeltager()
{
	int tempDeltagerNr = les("Deltagernummer", MINDELTAGERNUMMER, MAXDELTAGERNUMMER);
	char tempNavn[STRLEN];
	Deltager* tempDeltager = nullptr;

	if (deltagerListe->inList(tempDeltagerNr))
	{
		std::cout << "\nDeltagernummeret er allerede i bruk.\n";
	}
	else
	{
		do
		{
			les("Deltagerens navn", tempNavn);
			if (finnesNavn(tempNavn))
			{
				std::cout << "\nEn deltager med det navnet finnes allerede!\n";
			}
		} while (finnesNavn(tempNavn));
		tempDeltager = new Deltager(tempDeltagerNr, tempNavn);
		deltagerListe->add(tempDeltager);
	}
}


bool Deltagere::finnesNavn(char* id)
{
	Deltager* tempDeltager;
	bool fantDeltager = false;
	for (int i = 1; i <= deltagerListe->noOfElements(); i++)
	{
		tempDeltager = (Deltager*)deltagerListe->removeNo(i);
		if (!(strcmp(id, tempDeltager->getNavn())))
		{
			fantDeltager = true;
		}
		deltagerListe->add(tempDeltager);
	}
	return fantDeltager;
}


void Deltagere::skrivHoveddata()
{
	Deltager* tempDeltager;
    if (!deltagerListe->noOfElements())
    {
        std::cout << "\nDet ligger ingen deltagere i datastrukturen.\n"
            << "Ingenting aa skrive ut.\n";
    }
    else
    {
        for (int i = 1; i <= deltagerListe->noOfElements(); i++)
        {
            tempDeltager = (Deltager*)deltagerListe->removeNo(i);
            tempDeltager->displayHoveddata();
            deltagerListe->add(tempDeltager);
        }
    }
}


void Deltagere::skrivDeltager()
{
	Deltager* tempDeltager = finnDeltager();
	tempDeltager->display();
	deltagerListe->add(tempDeltager);
}


void Deltagere::skrivDeltagerFraOvelse(int id)
{
	Deltager* tempDeltager = (Deltager*)deltagerListe->remove(id);
	tempDeltager->displayOvelseData();
	deltagerListe->add(tempDeltager);
}


Deltager* Deltagere::finnDeltager()
{
	int valg;
	Deltager* tempDeltager = nullptr;
	valg = les("Angi deltager ved nummer (1) eller navn (2)?", 1, 2);
	if (valg == 1)
	{
		do
		{
			int tempNr = les("Skriv deltagernummer", MINDELTAGERNUMMER, MAXDELTAGERNUMMER);
			tempDeltager = (Deltager*)deltagerListe->remove(tempNr);	//	Returnerer nullptr hvis deltageren ikke er i lista
			if (!tempDeltager)
			{
				std::cout << "\nDeltagernummeret er ikke i bruk!\n";
			}
		} while (!tempDeltager);
	}
	else
	{
		char tempNavn[STRLEN];
		bool liktNavn;
		do
		{
			les("Deltagerens navn", tempNavn);
			liktNavn = false;
			int i = 1;
			while (!liktNavn && i <= deltagerListe->noOfElements())
			{
				tempDeltager = (Deltager*)deltagerListe->removeNo(i++);
				if (strcmp(tempNavn, tempDeltager->getNavn()))		//	Hvis navnene ikke er like. Husk at strcmp returnerer 0 om de er like
				{
					deltagerListe->add(tempDeltager);				//	Feil deltager, legg tilbake
				}
				else												//	Navnene er like
				{
					liktNavn = true;								//	
				}
			}
			if (!liktNavn)
			{
				std::cout << "\nFant ingen deltager ved det navnet!\n";
			}
		} while (!liktNavn);
	}
	return tempDeltager;
}


void Deltagere::endreEnDeltager()
{
	Deltager* tempDeltager = finnDeltager();
	tempDeltager->endreData();
	deltagerListe->add(tempDeltager);
}


void Deltagere::display()
{
	deltagerListe->displayList();
}

//	Skriver ut alle deltagere tilhorende en nasjon gitt ved nasjonId
void Deltagere::displayNasjonTropp()
{
	int antallDeltagere = deltagerListe->noOfElements();
	char nasjonsId[4];
	Deltager* tempDeltager;

	do
	{
		lesID("Nasjonens forkortelse", nasjonsId);	//	Sjekker at nasjonsforkortelsen

        if (!(nasjonbase.finnesID(nasjonsId)))
        {
            std::cout << "\nDenne nasjonen eksisterer ikke i systemet. ";
        }

	} while (!(nasjonbase.finnesID(nasjonsId)));	//	faktisk finnes i systemet

	for (int i = 1; i <= antallDeltagere; i++)
	{
		tempDeltager = (Deltager*)deltagerListe->removeNo(i);
		if (!(strcmp(nasjonsId, tempDeltager->getNasjonsId())))
		{
			tempDeltager->display();
		}
		deltagerListe->add(tempDeltager);
	}
}


void Deltagere::skrivTilFil()
{
	Deltager* tempDeltager = nullptr;
	int antallDeltagere = 0;
	antallDeltagere = deltagerListe->noOfElements();

	std::ofstream utfil("DELTAGERE.DTA");

	utfil << antallDeltagere << "\n\n";

	for (int i = 1; i <= antallDeltagere; i++)
	{
		tempDeltager = (Deltager*)deltagerListe->removeNo(i);
		tempDeltager->skrivTilFil(utfil);
		deltagerListe->add(tempDeltager);
	}
}


void Deltagere::lesFraFil()
{
	int antallDeltagere = 0;
	Deltager* tempDeltager = nullptr;

	int tempNr = 0;
	char tempId[STRLEN];

	std::ifstream innfil("DELTAGERE.DTA");
	innfil >> antallDeltagere; innfil.ignore(2);

	for (int i = 1; i <= antallDeltagere; i++)
	{
		innfil >> tempNr; innfil.ignore();
		innfil.getline(tempId, STRLEN);
		tempDeltager = new Deltager(tempNr, tempId, innfil);
		deltagerListe->add(tempDeltager);
	}
}


int Deltagere::finnesDeltagerID(int deltagerID)
{
	Deltager* deltagerPtr = nullptr;
	int antallDeltagere = deltagerListe->noOfElements();

	for (int i = 1; i <= antallDeltagere; i++)
	{
		deltagerPtr = (Deltager*)deltagerListe->removeNo(i);
		deltagerListe->add(deltagerPtr);

		if (deltagerListe->inList(deltagerID))
		{
			return true;
		}
	}
	return false;
}

const char* Deltagere::getDeltagersNasjonsID(int deltagerID)
{   
    Deltager* deltagerPtr = nullptr;

    deltagerPtr = (Deltager*)deltagerListe->remove(deltagerID);
    if (deltagerPtr)
    {
        deltagerListe->add(deltagerPtr);
    }

    return (deltagerPtr->getNasjonsId());
}
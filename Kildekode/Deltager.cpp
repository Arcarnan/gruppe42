#include "Deltager.h"
#include "Deltagere.h"
#include <iostream>
#include <cstring>

extern Nasjoner nasjonbase;
extern Deltagere deltagerbase;

//	Deltager-klassens funksjonsinnmat

Deltager::Deltager()
{
    std::cout << "\nDette er en tom constructor for Deltager, som ikke er ment aa brukes. ";
}


Deltager::Deltager(int nr, char* id) : NumElement(nr)
{
    char tempTrivia[STRLEN * 4];
    int tempKjonn = 0;
    int tempLen = 0;

    tempLen = strlen(id) + 1;
    navn = new char[tempLen];
    strcpy_s(navn, tempLen, id);

    do
    {
        lesID("Nasjonens forkortelse", nasjonsId);	//	Sjekker at nasjonsforkortelsen
    } while (!(nasjonbase.finnesID(nasjonsId)));	//	faktisk finnes i systemet
    nasjonbase.nyDeltager(nasjonsId);
    tempKjonn = les("(0) gutt, (1) jente", 0, 1);
    kjonn = (KJONN)tempKjonn;

    les("Generelle data om deltageren", tempTrivia, STRLEN * 4);
    tempLen = strlen(tempTrivia) + 1;
    trivia = new char[tempLen];
    strcpy_s(trivia, tempLen, tempTrivia);
}


Deltager::Deltager(int nr, char* id, std::ifstream& inn) : NumElement(nr)
{
    char tempGenData[STRLEN * 4];
    int tempLen = 0;
    int tempKjonn = 0;

    tempLen = strlen(id) + 1;
    navn = new char[tempLen];
    strcpy_s(navn, tempLen, id);

    inn.getline(nasjonsId, NASJONIDLENGDE + 1);
    nasjonbase.nyDeltager(nasjonsId);

    inn >> tempKjonn; inn.ignore();		//	kjonn staar som 0 eller 1 i filen, maa derfor
    kjonn = (KJONN)tempKjonn;			//	leses som int og castes til KJONN.

    inn.getline(tempGenData, STRLEN * 4); inn.ignore();		//	Er et ekstra linjeskift mellom objekter, derfor baade getline og ignore()
    tempLen = strlen(tempGenData) + 1;
    trivia = new char[tempLen];
    strcpy_s(trivia, tempLen, tempGenData);
}


Deltager::~Deltager()
{
    delete[] navn;
    delete[] trivia;
}


const char* Deltager::getNasjonsId()
{
    return nasjonsId;
}


const char* Deltager::getNavn()
{
    return navn;
}


void Deltager::display()
{
    std::cout << "\nDeltagernr:       " << number;
    std::cout << "\nFullt navn:       " << navn;
    std::cout << "\nNasjonalitet:     " << nasjonsId;
    std::cout << "\nKjonn:            " << kjonn;
    std::cout << "\nTrivia:           " << trivia;
    std::cout << "\n\n";
}


void Deltager::endreData()
{
    char valg = ' ';
    display();
    do
    {
        std::cout << "Hva vil du redigere\n?";
        std::cout << "\t(1) Fullt navn\n";
        std::cout << "\t(2) Nasjonalitet\n";
        std::cout << "\t(3) Kjonn\n";
        std::cout << "\t(4) Trivia\n";
        std::cout << "\t(X) Avslutte redigering\n";
        valg = les("Ditt valg");
        if (valg != 'X')
        {
            switch (valg)
            {
            case '1':		//	Fullt navn
            {
                char tempNavn[STRLEN];
                int tempLen = 0;

                les("Nytt navn", tempNavn);
                tempLen = strlen(tempNavn) + 1;
                delete[] navn;
                navn = new char[tempLen];
                strcpy_s(navn, tempLen, tempNavn);
                break;
            }
            case '2':		//	Nasjonalitet
            {
                do
                {
                    lesID("Nasjonens forkortelse", nasjonsId);	//	Sjekker at nasjonsforkortelsen
                } while (!(nasjonbase.finnesID(nasjonsId)));	//	faktisk finnes i systemet
                break;
            }
            case '3':		//	kjonn
            {
                int tempKjonn = les("(0) gutt, (1) jente", 0, 1);
                kjonn = (KJONN)tempKjonn;
                break;
            }
            case '4':		//	Trivia
            {
                char tempTrivia[STRLEN * 4];
                int tempLen = 0;
                les("Generelle data om deltageren", tempTrivia, STRLEN * 4);
                tempLen = strlen(tempTrivia) + 1;
                trivia = new char[tempLen];
                strcpy_s(trivia, tempLen, tempTrivia);
                break;
            }
            default:
            {
                display();
            }
            }
        }
    } while (valg != 'X');
}


void Deltager::displayHoveddata()
{
    std::cout << "\nDeltagernr:       " << number;
    std::cout << "\nFullt navn:       " << navn;
    std::cout << "\nKjonn:            " << kjonn;
    std::cout << '\n';
}


void Deltager::displayOvelseData()
{
    std::cout << "\n\tDeltagernr:       " << number;
    std::cout << "\n\tFullt navn:       " << navn;
    std::cout << "\n\tNasjonalitet:     " << nasjonsId;
}

void Deltager::skrivTilFil(std::ofstream& ut)
{
    ut << number << '\n'
        << navn << '\n'
        << nasjonsId << '\n'
        << kjonn << '\n'		//	Skriver 0 eller 1, maa leses inn som int og castes til KJONN
        << trivia << "\n\n";

    /*
    int skrevetPaaRad;
    for (int i = 0; i < strlen(trivia); i++)
    {
        ut << trivia[i];
        skrevetPaaRad++;
        if (skrevetPaaRad > (STRLEN * 0.9) && trivia[i] == ' ')
        {
            ut << '\n';
            skrevetPaaRad = 0;
        }
    }
    ut << "\n\n";
    */
}
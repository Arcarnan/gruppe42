#include "Nasjon.h"
#include "Hjelpefunksjoner.h"
#include <cstring>
#include <iostream>				//  cin, cout
#include <fstream>				//  ifstream, ofstream

Nasjon::Nasjon()
{
	std::cout << "\nDette er en tom constructor for Nasjon, som ikke er ment aa brukes. ";
}

Nasjon::Nasjon(char ID[NASJONIDLENGDE + 1]) : TextElement(ID)
{
	char tempStr[STRLEN];					//	Midlertidig char array
	char tempGenData[STRLEN * 4];			//	For generell trivia, mer plass
	int tempLen;							//	Char array'ens lengde

	les("Nasjonens fulle navn", tempStr);	//	Leser nasjonens navn
	tempLen = strlen(tempStr) + 1;			//	Maler lengden med \0
	nasjonNavn = new char[tempLen];			//	Allokerer minne
	strcpy_s(nasjonNavn, tempLen, tempStr);	//	Kopierer navnet til pekeren

	antDeltagere = 0;
																							
	les("Nasjonens kontaktperson", tempStr);//	Leser kontaktpersonens navn
	tempLen = strlen(tempStr) + 1;			//	Maler lengde med \0
	personNavn = new char[tempLen];			//	Allokerer minne
	strcpy_s(personNavn, tempLen, tempStr);	//	Kopierer navnet til pekeren

	personTlf = les("Kontakpersonens tlf-nr", 10000000, 99999999);

	les("Generelt om nasjonen", tempGenData);	//	Leser generell trivia
	tempLen = strlen(tempGenData) + 1;			//	Maler lengde med \0
	genData = new char[tempLen];				//	Allokerer minne
	strcpy_s(genData, tempLen, tempGenData);	//	Kopierer trivia til peker
}

Nasjon::Nasjon(std::ifstream & inn, char ID[NASJONIDLENGDE + 1]) : TextElement(ID)
{
	char tempStr[STRLEN];
	char tempGenData[STRLEN * 4];			//	For generell trivia, mer plass
	int tempLen;

	inn.getline(tempStr, STRLEN);
	tempLen = strlen(tempStr) + 1;			//	Maler lengde med \0
	nasjonNavn = new char[tempLen];			//	Allokerer minne
	strcpy_s(nasjonNavn, tempLen, tempStr);	//	Kopierer navnet til pekeren

	antDeltagere = 0;

	inn.getline(tempStr, STRLEN);
	tempLen = strlen(tempStr) + 1;			//	Maler lengde med \0
	personNavn = new char[tempLen];			//	Allokerer minne
	strcpy_s(personNavn, tempLen, tempStr);	//	Kopierer navnet til pekeren

	inn >> personTlf;
	inn.ignore();

	inn.getline(tempGenData, STRLEN * 4);
	tempLen = strlen(tempGenData) + 1;			//	Maler lengde med \0
	genData = new char[tempLen];				//	Allokerer minne
	strcpy_s(genData, tempLen, tempGenData);	//	Kopierer trivia til peker
}

Nasjon::~Nasjon()
{
	delete[] nasjonNavn;		//	Frigir minne
	delete[] personNavn;		//	Frigir minne
	delete[] genData;			//	Frigir minne
}

const char* Nasjon::hentNasjonID()
{
	return text;
}

void Nasjon::nyDeltager()
{		//	oppdaterer nasjonens antall deltagere nar en ny deltager registreres
	antDeltagere++;
}

void Nasjon::endreData()
{
	char kommando;
	char tempStr[STRLEN];					//	Midlertidig char array
	char tempGenData[STRLEN * 4];			//	For generell trivia, mer plass
	int tempLen;							//	Char array'ens lengde

	std::cout << "\n\t\tFolgende informasjon kan endres: \n";
	std::cout << "\n\t<N> - Nasjonens navn. ";
	std::cout << "\n\t<K> - Kontaktperson. ";
	std::cout << "\n\t<T> - TLF kontaktperson. ";
	std::cout << "\n\t<G> - Generel data. ";
	std::cout << "\n\t<X> - Avslutt endring. ";

	kommando = les("\nNasjon: Hva vil du endre?");						     //leser kommando fra bruker

	while (kommando != 'X')
	{
		switch (kommando)	//	gir bruker mulighet til a endre akkurat den tingen de onsker
		{
		case 'N':
			delete[] nasjonNavn;					//	sletter gamle for nye registreres
			les("Nytt fulle navn", tempStr);		//	Leser nasjonens navn
			tempLen = strlen(tempStr) + 1;			//	Maler lengden med \0
			nasjonNavn = new char[tempLen];			//	Allokerer minne
			strcpy_s(nasjonNavn, tempLen, tempStr);	//	Kopierer navnet til pekeren
			break;

		case 'K':
			delete[] personNavn;					//	sletter gamle for nye registreres
			les("Ny kontaktperson", tempStr);		//	Leser kontaktpersonens navn
			tempLen = strlen(tempStr) + 1;			//	Maler lengde med \0
			personNavn = new char[tempLen];			//	Allokerer minne
			strcpy_s(personNavn, tempLen, tempStr);	//	Kopierer navnet til pekeren
			break;

		case 'T':
			personTlf = les("Kontakpersonens tlf-nr", 10000000, 99999999);
			break;

		case 'G':
			delete[] genData;							//	sletter gamle for nye registreres
			les("Generelt om nasjonen", tempGenData);	//	Leser generell trivia
			tempLen = strlen(tempGenData) + 1;			//	Maler lengde med \0
			genData = new char[tempLen];				//	Allokerer minne
			strcpy_s(genData, tempLen, tempGenData);	//	Kopierer trivia til peker
			break;

		default:
			std::cout << "\nDette er desverre ikke et gyldig valg. \n";
			break;
		}
		std::cout << "\n\tNasjon under endring: " << text << ". \n";
		kommando = les("\nNasjon: Hva vil du endre?");						     //leser kommando fra bruker
	}
}

void Nasjon::display()		//	display alle data for en nasjon
{
	std::cout << "\n\nNasjon: " << text << ", \t" << nasjonNavn;
	std::cout << "\nNasjonen's deltagerantall: " << antDeltagere;
	std::cout << "\nNasjonen's Kontaktperson: " << personNavn << " - " << personTlf;
	std::cout << "\nVidere informasjon: " << genData;
}

void Nasjon::displayHoved()		//	display hoveddata for en nasjon
{
	std::cout << "\n\nNasjon: " << text << ", \t" << nasjonNavn;
	std::cout << "\nNasjonen's deltagerantall: " << antDeltagere;
}

void Nasjon::nasjonSkrivTilFil(std::ofstream & ut)
{
	ut <<"\n"<< text << "\n"		//	starter med \n for a ha tom linje mellom hver nasjon
		<< nasjonNavn << "\n"
		<< personNavn << "\n"
		<< personTlf << "\n"
		<< genData << "\n";
}		

/*
			FILFORMAT:
Antall nasjoner

Nasjon ID
Fullt Nasjonnavn
Kontaktperson navn
Kontaktperson tlf
Generell data om nasjon

Nasjon ID
Fullt Nasjonnavn
Kontaktperson navn
Kontaktperson tlf
Generell data om nasjon
*/

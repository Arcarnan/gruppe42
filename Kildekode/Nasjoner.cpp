#include <iostream>				//  cin, cout
#include <fstream>				//  ifstream, ofstream
#include <cstring>				//  strcpy, strlen, strncmp
#include "Nasjoner.h"
#include "konstanter.h"
#include "Hjelpefunksjoner.h"

extern Nasjoner nasjonbase;

//	Nasjoner-klassens funksjonsinnmat
Nasjoner::Nasjoner()
{
	nasjonerList = new List(Sorted);
}

void Nasjoner::nasjonerLesFraFil()
{
	char ID[3 + 1];
	int antallNasjoner;
	nasjonerList = new List(Sorted);
	Nasjon* nasjonPtr = nullptr;			//	------alltid sette alle pointers til = nullptr (eller {})

	std::ifstream inn("NASJONER.DTA");

	inn >> antallNasjoner;
	inn.ignore();

	if (inn)
	{		//	leser alle nasjoner fra fil
		for (int i = 1; i <= antallNasjoner; i++)
		{
			inn.ignore();
			inn.getline(ID, 4);
			nasjonPtr = new Nasjon(inn, ID);
			nasjonerList->add(nasjonPtr);
		}
	}
	else
	{
		std::cout << "\nKunne ikke finne filen 'NASJONER.DTA'. ";
	}
}

void Nasjoner::nasjonerSkrivTilFil()
{
	Nasjon* nasjonPtr = nullptr;			//	------alltid sette alle pointers til = nullptr (eller {})
	int antallNasjoner = nasjonerList->noOfElements();

	std::ofstream ut;
	ut.open("NASJONER.DTA");

	if (ut)
	{
		ut << antallNasjoner << "\n";

		for (int i = 1; i <= antallNasjoner; i++)
		{		//	skriver alle nasjoner til fil
			nasjonPtr = (Nasjon*)nasjonerList->removeNo(i);
			nasjonPtr->nasjonSkrivTilFil(ut);
			nasjonerList->add(nasjonPtr);
		}
	}
	else
	{
		std::cout << "\nKunne ikke finne filen 'NASJONER.DTA'. ";
	}
}

Nasjoner::~Nasjoner()
{
	delete nasjonerList;
}

void Nasjoner::display()
{
	Nasjon* nasjonPtr = nullptr;			//	------alltid sette alle pointers til = nullptr (eller {})
	char nasjonID[NASJONIDLENGDE+1];

	lesID ("Nasjonens ID (forkortelse)", nasjonID);			//	Leser nasjonens navn
	nasjonPtr = (Nasjon*)nasjonerList->remove(nasjonID);

	if (nasjonPtr)				//	true hvis ID finnes
	{		//	skriver ut alle data for en spesifikk nasjon
		nasjonPtr->display();
		nasjonerList->add(nasjonPtr);
	}
	else
	{
		std::cout << "\nDenne nasjonen finnes ikke i systemet. ";
	}
}

void Nasjoner::displayHoved()
{
	Nasjon* nasjonPtr = nullptr;			//	------alltid sette alle pointers til = nullptr (eller {})
	int antallNasjoner = nasjonerList->noOfElements();

	for (int i = 1; i <= antallNasjoner; i++)
	{
		nasjonPtr = (Nasjon*)nasjonerList->removeNo(i);
		nasjonPtr->displayHoved();
		nasjonerList->add(nasjonPtr);
	}
}

bool Nasjoner::finnesID(char* ID)
{
	Nasjon* nasjonPtr = nullptr;			//	------alltid sette alle pointers til = nullptr (eller {})
	int antallNasjoner = nasjonerList->noOfElements();

	for (int i = 1; i <= antallNasjoner; i++)
	{
		nasjonPtr = (Nasjon*)nasjonerList->removeNo(i);
		nasjonerList->add(nasjonPtr);

		if (!(strcmp(nasjonPtr->hentNasjonID(), ID)))				//	true hvis samme ID
		{
			return true;
		}
	}
	return false;
}

void Nasjoner::nyNasjon()
{
	Nasjon* nasjonPtr = nullptr;			//	------alltid sette alle pointers til = nullptr (eller {})
	char nasjonID[STRLEN];

	lesID("Nasjonens ID (forkortelse)", nasjonID);			//	Leser nasjonens navn

	if (finnesID(nasjonID))		//	kan evt. bare sette inn if (nasjonPtr)	
	{
		std::cout << "\nDenne nasjonen finnes allerede i systemet. ";
	}
	else
	{
		nasjonPtr = new Nasjon(nasjonID);
		nasjonerList->add(nasjonPtr);
	}
}

void Nasjoner::endreNasjon()
{
	Nasjon* nasjonPtr = nullptr;			//	------alltid sette alle pointers til = nullptr (eller {})
	char nasjonID[STRLEN];

	lesID("Nasjonens ID (forkortelse)", nasjonID);			//	Leser nasjonens navn
	nasjonPtr = (Nasjon*)nasjonerList->remove(nasjonID);

	if (nasjonPtr)											//	true hvis ID finnes
	{
		nasjonPtr->endreData();
		nasjonerList->add(nasjonPtr);
	}
	else
	{
		std::cout << "\nDenne nasjonen finnes ikke i systemet. ";
	}
}

void Nasjoner::nyDeltager(char* ID)
{				//	adderer antall deltagere for en nasjon med 1 hver gang en ny deltager for den nasjonen registreres
	Nasjon* nasjonPtr = (Nasjon*)nasjonerList->remove(ID);
	nasjonPtr->nyDeltager();
	nasjonerList->add(nasjonPtr);
}

/*
FILFORMAT:
Antall nasjoner

Nasjon ID
Fullt Nasjonnavn
Antall deltagere
Kontaktperson navn
Kontaktperson tlf
Generell data om nasjon

Nasjon ID
Fullt Nasjonnavn
Antall deltagere
Kontaktperson navn
Kontaktperson tlf
Generell data om nasjon
*/
#pragma once

#include "ListTool2B.h"
#include "Konstanter.h"
#include "Hjelpefunksjoner.h"
#include "Nasjoner.h"
#include <fstream>


//	Deltager-klassen, dens datamedlemmer og funksjonsheadinger

class Deltager : public NumElement
{
private:
	//	Deltagernummer lagres i NumElement
	char* navn;
	char nasjonsId[NASJONIDLENGDE+1];
	KJONN kjonn;
	char* trivia;
public:
	Deltager();		//	Skal egentlig aldri kjores, og kan aldri kjores slik programmet er na
	Deltager(int, char*);
	Deltager(int, char*, std::ifstream&);
	~Deltager();
	const char* getNasjonsId();
	const char* getNavn();
	virtual void display();
	void endreData();
	void displayHoveddata();
	void displayOvelseData();
	void skrivTilFil(std::ofstream&);
};

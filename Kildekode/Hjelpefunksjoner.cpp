#include <iostream>				//  cin, cout
#include <cstring>				//  strcpy, strlen, strncmp
#include <cctype>				//  toupper
#include <stdio.h>				//  isdigit
#include <stdlib.h>				//  atoi
#include <ctype.h>
#include "Hjelpefunksjoner.h"
#include "Poeng.h"
#include "Medaljer.h"

extern Poeng poengstatistikk;
extern Medaljer medaljestatistikk;

using namespace std;

char les(char* h)		            //	Leser ett tegn, hvis bokstav blir det stor bokstav
{
	char temp;						//	Midlertidig tegn
	cout << "\n" << h << ":  ";		//	Skriver hjelpetekst
	cin >> temp; cin.ignore();		//	Leser inn ett tegn
	return(toupper(temp));			//	Returnerer tegn, bokstav blir stor
}

int les(char* tekst, int min, int max)				//looper til numerisk input mellom min og max
{
	bool erTall;
	int omTall;
	char sjekk[MAXNUMERISK];

	do
	{
		do
		{
			cout << "\n" << tekst << " (" << min << '-' << max << "):  ";	//	Skriver hjelpetekst
			erTall = true;			//starter true for aa unnga evig loop
			cin.getline(sjekk, MAXNUMERISK);			//	leser inn verdien som skal testes

			for (int i = 0; sjekk[i] != '\0'; i += 1)
			{								//	sjekker om alle tegn er numeriske
				if (!isdigit(sjekk[i]))
				{			                //	om et eller flere tegn ikke er numerisk
					erTall = false;
				}
			}

			if (!erTall)
			{			                  //	om et eller flere tegn ikke er numerisk
				cout << "\nDette er ikke en positiv numerisk verdi. ";
			}
		} while (!erTall);

		omTall = atoi(sjekk);			//	bruker atoi for a caste char til int
	} while (omTall < min || omTall > max);
	return (omTall);				    //	returnerer numerisk tallverdi
}

void les(char* h, char* t, int len)		//	Leser en ikke-blank tekst, og setter forste bokstav uppercase og alle andre lowercase
{
    char temp[STRLEN*4];

	do
	{
		cout << "\n" << h << ":  ";		//	Skriver hjelpetekst
		cin.getline(temp, len);			//	Leser tekst
	} while (strlen(temp) == 0);		//	Sa lenge teksten er blank

    t[0] = (toupper(temp[0]));          //   setter forste bokstav til toupper
    for (int i = 1; i <= len; i++)      //  setter alle andre til tolower
    {
        t[i] = (tolower(temp[i]));
    }

    if (cin.fail())                         //  hvis det var for mange tegn
    {
        cin.clear();                        //  tillat operasjoner paa stream'en
        cin.ignore(INT_MAX, '\n');          //  Tom buffer
    }
}

void lesID(char* tekst, char ID[NASJONIDLENGDE])

{
	char temp[NASJONIDLENGDE+1];

	do
	{
		cout << "\n" << tekst << ":  ";		                //	Skriver hjelpetekst
		cin.getline(temp, NASJONIDLENGDE +1);				//	Leser tekst
	} while (strlen(temp) < NASJONIDLENGDE || strlen(temp) > NASJONIDLENGDE);			//	Sa lenge teksten er blank

	for (int i = 0; i <= NASJONIDLENGDE; i++)
	{
		ID[i] = (toupper(temp[i]));
	}
}

void oppdaterStatistikk(char poengNasjonsForkortelser[MAXNASJONER + 1][NASJONIDLENGDE + 1], int* poeng, int& poengsNasjoner, int& antallMedPoeng, char medaljeNasjonsForkortelser[MAXNASJONER + 1][NASJONIDLENGDE + 1], int medaljer[MAXNASJONER + 1][3], int& medaljersNasjoner, int& antallNasjonerMedMedaljer)
{
    int antallUsorterte = poengsNasjoner;
    int tempIndeks = 0;
    int tempVerdi = 0;
    int antallSomSkalFjernes = 0;
    int forsteUsorterte = 0;

    for (int i = 1; i <= poengsNasjoner; i++)
    {
        strcpy_s(medaljeNasjonsForkortelser[i], NASJONIDLENGDE + 1, poengNasjonsForkortelser[i]);
    }
    medaljersNasjoner = poengsNasjoner;

    while (antallUsorterte)
    {
        tempVerdi = 0;
        ++forsteUsorterte;

        for (int i = forsteUsorterte; i <= (forsteUsorterte + antallUsorterte - 1); i++)
        {
            if (poeng[i] > tempVerdi)
            {
                tempIndeks = i;
            }
        }

        std::swap(poeng[tempIndeks], poeng[poengsNasjoner - antallUsorterte + 1]);
        std::swap(poengNasjonsForkortelser[tempIndeks], poengNasjonsForkortelser[poengsNasjoner - antallUsorterte + 1]);
        std::swap(medaljer[tempIndeks], medaljer[medaljersNasjoner - antallUsorterte + 1]);
        std::swap(medaljeNasjonsForkortelser[tempIndeks], medaljeNasjonsForkortelser[medaljersNasjoner - antallUsorterte + 1]);
        --antallUsorterte;
    }

    for (int i = 1; i <= poengsNasjoner; i++)
    {
        if (!poeng[i])
        {
            ++antallSomSkalFjernes;
        }
    }
    poengsNasjoner -= antallSomSkalFjernes;
    antallMedPoeng -= antallSomSkalFjernes;
    medaljersNasjoner -= antallSomSkalFjernes;
    antallNasjonerMedMedaljer -= antallSomSkalFjernes;

    antallNasjonerMedMedaljer = antallMedPoeng;
}
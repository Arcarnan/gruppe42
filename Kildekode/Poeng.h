#pragma once		//erstatter "if !defined" --- "define"
#include "Statistikk.h"

class Poeng : public Statistikk
{
private:
	int poeng[MAXNASJONER+1];
	int antMedPoeng;
public:
	Poeng();
	//void lesFraFil();
	void skrivTilFil();
	void poengOversikt();
    void redigerResultater(int deltagere[MAXDELTAGERE], bool leggeTil);
    void sorterLister();
};

